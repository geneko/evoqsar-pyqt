IF EXIST "build" (
	rmdir /S /Q build
)

IF EXIST "dist" (
	rmdir /S /Q dist
)

call activate evoqsar
pyinstaller windows-debug.spec