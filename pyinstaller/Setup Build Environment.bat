REM Create the evoqsar environment with Python 2.7 and Anaconda 2.5 libraries
conda create -y -n evoqsar python=2.7 anaconda=2.5

REM Activate evoqsar environment
call activate evoqsar

REM Install packages required for evoQSAR
conda install -y evoqsar=1.0 -c geneko
pip install pyinstaller==3.1.1 --no-deps

REM Downgrade packages
conda install -y pyqt=4.10
conda install -y setuptools=19.2