# README #

This is the source code repository for the PyQt GUI of the [evoQSAR library](https://bitbucket.org/geneko/evoqsar), developed by Eric Su for his [M.S. Thesis in Computational Science at San Diego State University](http://hdl.handle.net/10211.3/163439), and maintained by Gene Ko.

## Dependencies ##
See */pyinstaller/Build Instructions.txt* for detailed commentary regarding dependencies.