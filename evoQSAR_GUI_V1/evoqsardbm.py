""" Stores all user-entered values  """

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = ['Gene Ko']
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

import pickle
import pprint

class dbm(object):

    def __init__( self, parent = None ):
        self.initialStartup()

    def initialStartup(self):
        print "initializing dbm..."
        # Initialize all the variables
        ################################################################################################################
        # debug
        # self.m_featFreq = [(191, 79), (24, 79), (6, 79), (244, 79), (25, 79), (174, 78), (130, 46), (348, 24), (122, 22), (175, 11), (319, 11), (158, 10), (365, 10), (80, 9), (239, 8), (19, 8), (320, 7), (318, 6), (373, 5), (11, 4), (377, 4), (173, 3), (159, 3), (14, 3), (23, 3), (89, 3), (213, 3), (88, 2), (137, 2), (350, 2), (134, 2), (144, 2), (356, 2), (83, 2), (140, 2), (166, 2), (49, 2), (262, 2), (12, 2), (156, 2), (168, 2), (110, 2), (242, 2), (240, 2), (160, 2), (372, 2), (82, 1), (259, 1), (347, 1), (132, 1), (157, 1), (171, 1), (253, 1), (382, 1), (169, 1), (86, 1), (375, 1), (124, 1), (53, 1), (261, 1), (282, 1), (96, 1), (222, 1), (249, 1), (176, 1), (127, 1), (125, 1), (241, 1), (263, 1), (202, 1), (360, 1), (367, 1), (342, 1), (232, 1), (354, 1), (43, 1), (45, 1), (151, 1)]

        # test values for FS
        # self.m_training_dataset = "C:/Users/ESU_2/Dropbox/GIT/evoQSARwGUI/test/HIV_Train.csv"
        # self.m_validation_dataset = "C:/Users/ESU_2/Dropbox/GIT/evoQSARwGUI/test/HIV_Valid.csv"
        # self.m_test_dataset = "C:/Users/ESU_2/Dropbox/GIT/evoQSARwGUI/test/HIV_Test.csv"

        # test values for single qsar MLR
        # self.m_training_dataset = "C:/Users/ESU_2/Desktop/HIV/MLR/HIV_Train_mod.csv"
        # # self.m_validation_dataset = "C:/Users/ESU_2/Dropbox/GIT/evoQSARwGUI/test/HIV_Valid.csv"
        # self.m_test_dataset = "C:/Users/ESU_2/Desktop/HIV/MLR/HIV_Test_mod.csv"

        # # test values for single qsar MLR - gene's updated code
        # self.m_training_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\MLR\HIV_Train_MLR.csv"
        # self.m_test_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\MLR\HIV_Test_MLR.csv"
        # self.m_singleqsar_prediction_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\MLR\NCI_MLR.csv"

        # # test values for single qsar PLSR - gene's updated code
        # self.m_training_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\PLS\HIV_Train_PLS.csv"
        # self.m_test_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\PLS\HIV_Test_PLS.csv"
        # self.m_singleqsar_prediction_dataset = "C:\Users\ESU_2\Desktop\Eric-SingleQSAR\Eric-SingleQSAR\PLS\NCI_PLS.csv"

        # test values for stepwise qsar
        # self.m_training_dataset = "C:/Users/ESU_2/Desktop/Stepwise/HIV_Train.csv"
        # self.m_validation_dataset = "C:/Users/ESU_2/Desktop/Stepwise/HIV_Validation.csv"
        # self.m_test_dataset = "C:/Users/ESU_2/Desktop/Stepwise/HIV_Test.csv"
        ################################################################################################################


        # Initialize variables with default values
        self.maxTabIdx = 10

        self.m_featFreq = []
        self.m_featurenames_flag = False
        self.m_testscript_flag = False

        self.gui_mode = ""

        self.m_dataset_header = None
        self.m_dataset_idxcol = None
        self.m_dataset_std = False
        self.m_dataset_rescale = False

        self.m_feature_selection = "DE-BPSO"
        self.m_alpha1_param = 0.5
        self.m_alpha2_param = 0.33
        self.m_beta_param = 1.5
        self.m_f_param = 0.8
        self.m_cr_param = 0.9

        self.m_initfeat_param = 3
        self.m_popsize_param = 20
        self.m_maxgen_param = 2000
        self.m_maxmodels_param = 100
        self.m_penaltyfactor_param = 2
        self.m_parallel_param = -1

        self.m_modeling_method = "MLR"
        self.m_n_components = 2
        self.m_cv_nfold_param = 2
        self.m_cv_shuffle_chkbox = None
        self.m_cv_shuffle_nruns_param = 1
        self.m_paramcv_dict = {'nFold': -1}
        self.m_parallel_param = -1

        self.m_dataset_header_2 = None
        self.m_dataset_idxcol_2 = None
        self.m_max_descript = 20

        self.figure = None
        self.layout_enabled = False

        self.m_singleqsar_yrand_nruns = 100
        self.m_yRand_user_input = False

        self.m_singleqsar_prediction_header = None
        self.m_singleqsar_prediction_IdxCol = None


        # enabled tabs
        self.enabledTabs = [True, False, False, False, False, False, False, False, False, False, False,]


        # enabled buttons
        self.ui_bool_trainingbrowse_btn = False
        self.ui_bool_validationbrowse_btn = False
        self.ui_bool_testbrowse_btn = False
        self.ui_bool_trainingset_txtline = False
        self.ui_bool_validationset_txtline = False
        self.ui_bool_testset_txtline = False
        self.ui_bool_alpha_txtline_1 = False
        self.ui_bool_alpha_txtline_2 = False
        self.ui_bool_beta_txtline = False
        self.ui_bool_f_txtline = False
        self.ui_bool_cr_txtline = False
        self.ui_bool_ncomponents_txtline = False
        self.ui_bool_cv_nfold_txtline = False
        self.ui_bool_cv_shuffle_chkbox = False
        self.ui_bool_cv_shuffle_nruns_txtline = False
        self.ui_bool_results_models_outputfile_txtedit = False
        self.ui_bool_results_importance_outputfile_txtedit = False
        self.ui_bool_testset_txtline_2 = False
        self.ui_bool_stepwise_run_btn = False
        self.ui_bool_stepwise_outputfile_txtline = False
        self.ui_bool_singleqsar_ad_trainoutputfile_txtline = False
        self.ui_bool_singleqsar_ad_testoutputfile_txtline = False
        self.ui_bool_singleqsar_ad_init_btn = False
        self.ui_bool_singleqsar_yrand_outputfile_btn = False
        self.ui_bool_singleqsar_yrand_init_btn = False
        self.ui_bool_singleqsar_yrand_outputfile_txtline = False
        self.ui_bool_singleqsar_statistics_outputfile_txtline = False
        self.ui_bool_singleqsar_prediction_dataset_txtline = False
        self.ui_bool_singleqsar_prediction_outputfile_txtline = False
        self.ui_bool_singleqsar_prediction_run_btn = False


        # textline values
        self.m_training_dataset = ""
        self.m_validation_dataset = ""
        self.m_test_dataset = ""
        self.m_results_models_outputfile = ""
        self.m_results_importance_outputfile = ""
        self.m_test_dataset_2 = ""
        self.m_stepwise_outputfile = ""
        self.m_singleqsar_ad_trainoutputfile = ""
        self.m_singleqsar_ad_testoutputfile = ""
        self.m_singleqsar_yrand_outputfile = ""
        self.m_singleqsar_statistics_outputfile = ""
        self.m_singleqsar_outputStats = ""
        self.m_singleqsar_prediction_dataset = ""
        self.m_singleqsar_prediction_outputfile = ""


        # combobox values
        self.m_dataset_extra = ""
        self.m_paramcv_value = ""



def save_object(obj, filename):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)
        print "Saved Object - " + str(filename)

def load_object(filename):
    with open(filename, 'rb') as input:
        loaded_object = pickle.load(input)
        print "Loaded Object - " + str(filename)
    return loaded_object



