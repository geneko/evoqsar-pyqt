__author__ = 'ESU_2'

"""
evoQSAR is a drug discovery toolkit used quantitative structure-activity relationship (QSAR) analysis.
This is a graphical user interface that resides above the evoQsAR codebase to allow for easy access and
parameter entry into the evoQSAR toolkit.

This GUI is coded using Qt Designer and PyQt.

It provides a lightweight, simple and consistent, wizard based navigation
system that will run on all major desktop operating systems.

"""

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = []
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

# define version information
__requires__        = ['PyQt4']
__version_info__    = (0, 0, 0)
__version__         = 'v%i.%02i.%02i' % __version_info__
__revision__        = __version__