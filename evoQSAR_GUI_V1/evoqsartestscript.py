from __future__ import division

""" Runs test script for evoqsar. """

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = ['Gene Ko']
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

import time
import numpy as np
import pandas as pd
import csv


from evoqsar import EA, Model

def evoQSARTestScript(obj):

    # Get values
    modelingMethod = obj.m_modeling_method
    plsrNComponents = obj.m_n_components
    featureSelectionMethod = obj.m_feature_selection
    initialFeatures = obj.m_initfeat_param
    populationSize = obj.m_popsize_param
    maxGenerations = obj.m_maxgen_param
    maxModels = obj.m_maxmodels_param
    paramCV = obj.m_paramcv_dict
    paramFitness = {}
    paramFitness['c'] = obj.m_penaltyfactor_param
    parallel = obj.m_parallel_param
    xTrain = obj.m_training_dataset_x
    yTrain = obj.m_training_dataset_y
    xValidation = obj.m_validation_dataset_x
    yValidation = obj.m_validation_dataset_y

    # Define Mathematical Model to use
    if modelingMethod == "PLSR":
        model = Model(modelingMethod, plsrNComponents)
    else: # MLR Case
        model = Model(modelingMethod)

    hiv = EA(model, xTrain, yTrain, xValidation, yValidation, initFeat=initialFeatures,
             paramCV=paramCV, popSize=populationSize, paramFitness = paramFitness, maxGen=maxGenerations, maxModels=maxModels, parallel=parallel)

    start = time.time()
    if featureSelectionMethod == 'BPSO':
        alpha1 = obj.m_alpha1_param
        alpha2 = obj.m_alpha2_param
        beta = obj.m_beta_param
        hiv.bpso(alpha=(alpha1, alpha2), beta=beta)
    if featureSelectionMethod == 'DE-BPSO':
        alpha1 = obj.m_alpha1_param
        alpha2 = obj.m_alpha2_param
        beta = obj.m_beta_param
        f = obj.m_beta_param
        cr = obj.m_cr_param
        hiv.debpso(alpha=(alpha1, alpha2), beta=beta, f=f, cr=cr)
    end = time.time()
    print 'Elapsed Time: ' + str((end-start)/60)

    if obj.m_dataset_header == 0:
        obj.m_featFreq = hiv.importance() # [ [descriptor column idx, occurrence count], [], [], [] ]
        obj.m_featurenames_flag = True
        print ("\n*** featFreq *** :: is defined")
    else:
        obj.m_featFreq = hiv.importance()    # Output feature importance w/ descriptor names
        obj.m_featurenames_flag = False
        print ("\n*** featFreq *** :: is NOT defined")

    obj.m_testscript_flag = True
    obj.m_model_object = hiv
    return True


def evoQSAROutputModels(obj):
    outputFilename = obj.m_results_models_outputfile
    modelObject = obj.m_model_object
    try:
        modelObject.toFile(outputFilename)
        return True
    except Exception:
        print ("Error: evoQSAROutputModels")
        return False


def evoQSAROutputImportance(obj):
    outputFilename = obj.m_results_importance_outputfile
    modelObject = obj.m_model_object
    featureNames = obj.m_featurenames
    try:
        if obj.m_dataset_header == 0:
            modelObject.importance(outputFilename, names=featureNames)
        else:
            modelObject.importance(outputFilename)
        return True
    except Exception:
        print ("Error: evoQSAROutputImportance")
        return False


def evoQSARStepWise(obj):

    importance = obj.m_featFreq
    # importance = [(145, 1001), (56, 998), (191, 988), (156, 965), (287, 902), (351, 867), (16, 678), (17, 136), (283, 136), (284, 133), (189, 114), (266, 57), (210, 53), (332, 52), (250, 45), (197, 44), (348, 38), (151, 34), (2, 32), (161, 31), (39, 29), (288, 28), (44, 27), (174, 24), (290, 21), (355, 21), (202, 21), (302, 18), (190, 18), (160, 18), (385, 16), (159, 16), (97, 15), (328, 14), (187, 14), (158, 14), (18, 14), (231, 13), (349, 13), (386, 12), (353, 12), (345, 11), (12, 11), (99, 11), (277, 11), (350, 11), (70, 10), (285, 10), (144, 10), (292, 10), (274, 10), (354, 10), (21, 10), (333, 9), (207, 9), (268, 9), (167, 9), (356, 9), (310, 9), (68, 9), (245, 9), (26, 8), (331, 8), (4, 8), (346, 8), (318, 8), (46, 8), (1, 8), (147, 8), (73, 8), (297, 8), (9, 8), (259, 8), (170, 8), (306, 8), (316, 8), (34, 8), (256, 7), (327, 7), (78, 7), (352, 7), (61, 7), (374, 7), (246, 7), (92, 7), (301, 7), (303, 7), (279, 7), (50, 7), (378, 7), (253, 7), (275, 7), (242, 7), (179, 7), (192, 7), (343, 7), (96, 6), (314, 6), (0, 6), (293, 6), (93, 6), (60, 6), (19, 6), (132, 6), (232, 6), (140, 6), (14, 6), (74, 6), (131, 6), (25, 6), (371, 6), (75, 6), (175, 6), (173, 6), (323, 6), (15, 6), (5, 6), (344, 6), (162, 6), (114, 6), (300, 6), (51, 6), (43, 6), (307, 6), (69, 6), (45, 6), (63, 6), (169, 6), (359, 6), (55, 6), (133, 5), (204, 5), (357, 5), (146, 5), (163, 5), (235, 5), (309, 5), (165, 5), (203, 5), (57, 5), (52, 5), (62, 5), (372, 5), (81, 5), (87, 5), (13, 5), (85, 5), (7, 5), (286, 5), (59, 5), (249, 5), (262, 5), (180, 5), (305, 5), (375, 5), (176, 5), (31, 5), (150, 5), (270, 5), (360, 5), (272, 5), (112, 5), (3, 5), (295, 5), (100, 5), (48, 5), (33, 5), (382, 5), (312, 5), (40, 5), (66, 5), (252, 5), (49, 5), (342, 4), (247, 4), (377, 4), (340, 4), (241, 4), (299, 4), (64, 4), (178, 4), (370, 4), (154, 4), (29, 4), (54, 4), (194, 4), (228, 4), (28, 4), (23, 4), (336, 4), (338, 4), (339, 4), (35, 4), (36, 4), (236, 4), (182, 4), (119, 4), (233, 4), (110, 4), (107, 4), (117, 4), (103, 4), (149, 4), (108, 4), (138, 4), (24, 4), (27, 4), (330, 4), (20, 4), (324, 4), (10, 4), (362, 4), (206, 4), (326, 4), (214, 4), (313, 4), (71, 4), (184, 4), (289, 4), (387, 4), (200, 4), (166, 4), (129, 4), (325, 4), (143, 3), (134, 3), (102, 3), (109, 3), (115, 3), (369, 3), (104, 3), (38, 3), (98, 3), (358, 3), (238, 3), (317, 3), (32, 3), (111, 3), (77, 3), (311, 3), (230, 3), (164, 3), (319, 3), (234, 3), (94, 3), (123, 3), (6, 3), (304, 3), (90, 3), (212, 3), (91, 3), (282, 3), (243, 3), (185, 3), (47, 3), (248, 3), (244, 3), (383, 3), (11, 3), (381, 3), (321, 3), (294, 3), (361, 3), (388, 3), (186, 3), (199, 3), (281, 3), (177, 3), (183, 3), (265, 3), (196, 3), (193, 3), (322, 3), (213, 3), (260, 3), (364, 3), (366, 3), (341, 3), (334, 2), (335, 2), (128, 2), (67, 2), (221, 2), (251, 2), (37, 2), (157, 2), (363, 2), (225, 2), (291, 2), (195, 2), (168, 2), (101, 2), (315, 2), (254, 2), (224, 2), (222, 2), (65, 2), (379, 2), (188, 2), (136, 2), (89, 2), (181, 2), (201, 2), (148, 2), (219, 2), (84, 2), (116, 2), (218, 2), (72, 2), (88, 2), (373, 2), (79, 2), (239, 2), (171, 2), (269, 2), (308, 2), (263, 2), (22, 2), (267, 2), (135, 2), (155, 1), (152, 1), (376, 1), (153, 1), (211, 1), (120, 1), (280, 1), (380, 1), (365, 1), (58, 1), (298, 1), (384, 1), (240, 1), (264, 1), (347, 1), (258, 1), (86, 1), (276, 1), (271, 1), (142, 1), (121, 1), (95, 1), (125, 1), (126, 1), (227, 1), (337, 1), (118, 1), (226, 1), (237, 1), (113, 1), (76, 1), (141, 1), (320, 1), (105, 1), (205, 1), (130, 1), (106, 1), (139, 1), (329, 1), (215, 1), (53, 1), (223, 1), (8, 1), (41, 1), (261, 1)]
    maxDesc = int(obj.m_max_descript)
    modelname = obj.m_modeling_method
    plsrNComponents = obj.m_n_components
    xTrain = obj.m_training_dataset_x
    xValidation = obj.m_validation_dataset_x
    xTest = obj.m_test_dataset_2_x
    yTrain = obj.m_training_dataset_y
    yValidation = obj.m_validation_dataset_y
    yTest = obj.m_test_dataset_2_y
    stepwiseOutputFile = obj.m_stepwise_outputfile

    print "maxDesc=" + str(maxDesc)
    print type(maxDesc)
    print "stepwiseOutputFile=" + str(stepwiseOutputFile)
    print type(stepwiseOutputFile)

    if modelname == "PLSR": #PLSR
        minDesc = plsrNComponents
    else: # MLR Case
        minDesc = 2

    print "modelname = " + str(modelname)
    print "minDesc = " + str(minDesc)

    model = Model(modelname)

    if importance is None or len(importance) < minDesc:
        print 'Cannot perform stepwise'
    else:
        if len(importance) < maxDesc:
            maxDesc = len(importance)

        # Get top ranked indices
        impIdx = []
        for i in range(0, maxDesc):
            print "inside impIdx loop:" + str(i)
            impIdx.append(importance[i][0])
        print impIdx

        # Perform stepwise modeling
        stepNum = 1
        for i in range(minDesc, maxDesc+1):

            idx = impIdx[0:i]

            xTrainStepwise = xTrain[:, idx]
            xValidationStepwise = xValidation[:, idx]
            xTestStepwise = xTest[:, idx]

            model.train(xTrainStepwise, yTrain) # Training  set
            model.cv(nFold=-1)    # Cross-Validation
            model.test(xValidationStepwise, yValidation) # Validation set

            # Create header for output
            if stepNum == 1:
                header = ['Indices', 'R^2', 'Q^2', 'R^2_pred(v)']

            stats = [idx, model.r2_, model.q2_, model.r2Pred_]    # Get stats of training/cv/validation set

            # Test set + Tropsha statistis
            model.test(xTestStepwise, yTest)
            if stepNum == 1:    # Add to header
                header.append('R^2_pred(t)')
                tropshaKeys = model.tropsha_.keys()   # Grab Tropsha Keys (maintains order)
                for key in tropshaKeys:
                    header.append(key)

            stats.append(model.r2Pred_)   # Append R^2_pred(t) of test sets

            for key in tropshaKeys: # Append Tropsha stats
                stats.append(model.tropsha_[key])

            # Append statistics into Pandas DF
            if stepNum == 1:
                df = pd.DataFrame(columns=header)
                df = df.append(pd.Series(data=stats, index=header, name=stepNum))
            else:
                df = df.append(pd.Series(data=stats, index=header, name=stepNum))

            stepNum += 1

        # Dump stepwise stats to a file
        df.to_csv(stepwiseOutputFile)
        print "StepWise QSAR Completed!!!!"


def evoQSARSingle(obj):

    # Get Values
    modelingMethod = obj.m_modeling_method
    paramFitness = {}
    paramFitness['c'] = obj.m_penaltyfactor_param
    xTrain = obj.m_training_dataset_x
    yTrain = obj.m_training_dataset_y
    xTest = obj.m_test_dataset_x
    yTest = obj.m_test_dataset_y

    # initialize values
    outputStats = ""

    print ("\n***evoqsartestscript - evoQSARSingle***\n")


    # Define Mathematical Model to use
    if modelingMethod == "PLSR": # PLSR Case

        # PLSR Modeling
        plsrNComponents = obj.m_n_components
        pls = Model('PLSR', n_components=plsrNComponents)
        pls.train(xTrain, yTrain)   # Train Model
        pls.cv(nFold=1) # Leave-One-Out Cross-Validation

        # Get predicted values of training and validation set.
        yHatTrain = pls.trainYHat_

        # Print Model Coefficients
        print('Coefficients: ' + str(pls.model.coef_))
        outputStats += ('Coefficients: ' + str(pls.model.coef_) + '\n\n')

        # Print statistics
        print('R^2 = ' + str(pls.r2_))
        print('Q^2 = ' + str(pls.q2_))
        outputStats += ('R^2 = ' + str(pls.r2_) + '\n')
        outputStats += ('Q^2 = ' + str(pls.q2_) + '\n')

        # Test set
        pls.test(xTest, yTest)    # Retrain w/ test set
        yHatTest = pls.testYHat_
        print('R^2_pred(t) = ' + str(pls.r2Pred_))
        outputStats += ('R^2_pred(t) = ' + str(pls.r2Pred_) + '\n')

        print('\nTropsha Metrics')
        outputStats += ('\nTropsha Metrics' + '\n')

        for key in pls.tropsha_.keys():
            print(key + ' = ' + str(pls.tropsha_[key]))
            outputStats += (key + ' = ' + str(pls.tropsha_[key]) + '\n')

        obj.m_singleqsar_model = pls
        print "\n*** Location: Single QSAR - PLSR - End***\n"


    else: # MLR Case
        # MLR Modeling
        print "\n*** Location: Single QSAR - MLR - Start***\n"
        mlr = Model('MLR')
        mlr.train(xTrain, yTrain)   # Train Model
        mlr.cv(nFold=1) # Leave-One-Out Cross-Validation

        # Get predicted values of training and validation set.
        yHatTrain = mlr.trainYHat_

        # Print Model Coefficients
        print('Intercept: ' + str(mlr.model.intercept_))
        print('Coefficients: ' + str(mlr.model.coef_))
        outputStats += ('Intercept: ' + str(mlr.model.intercept_) + '\n')
        outputStats += ('Coefficients: ' + str(mlr.model.coef_) + '\n\n')

        # Print statistics
        print('R^2 = ' + str(mlr.r2_))
        print('Q^2 = ' + str(mlr.q2_))
        outputStats += ('R^2 = ' + str(mlr.r2_) + '\n')
        outputStats += ('Q^2 = ' + str(mlr.q2_) + '\n')

        mlr.test(xTest, yTest)    # Retrain w/ test set
        yHatTest = mlr.testYHat_
        print('R^2_pred(t) = ' + str(mlr.r2Pred_))
        outputStats += ('R^2_pred(t) = ' + str(mlr.r2Pred_) + '\n')

        print('\nTropsha Metrics')
        outputStats += ('\nTropsha Metrics' + '\n')

        for key in mlr.tropsha_.keys():
            print(key + ' = ' + str(mlr.tropsha_[key]))
            outputStats += (key + ' = ' + str(mlr.tropsha_[key]) + '\n')

        obj.m_singleqsar_model = mlr
        print "\n*** Location: Single QSAR - MLR - End***\n"

    obj.m_singleqsar_yHatTrain = yHatTrain
    obj.m_singleqsar_yHatTest = yHatTest
    obj.m_singleqsar_outputStats = outputStats
    return True


def evoQSARAD(obj):

    xTrain = obj.m_training_dataset_x
    xTest = obj.m_test_dataset_x
    yTrain = obj.m_training_dataset_y
    yTest = obj.m_test_dataset_y
    yHatTrain = obj.m_singleqsar_yHatTrain
    yHatTest = obj.m_singleqsar_yHatTest
    dataTrain = obj.m_training_dataset_TrainX
    dataTest = obj.m_test_dataset_TestX
    aDTrainOutputFile = obj.m_singleqsar_ad_trainoutputfile
    aDTestOutputFile = obj.m_singleqsar_ad_testoutputfile

    ################ WILLIAMS PLOT (LEVERAGE)/APPLICABILITY DOMAIN
    # Compute the residuals
    residualTrain = yTrain - yHatTrain
    residualTest = yTest - yHatTest

    # Normalize the residuals
    residualTrainNorm = (residualTrain - residualTrain.mean())/residualTrain.std()
    residualTestNorm = (residualTest - residualTrain.mean())/residualTrain.std()

    # Compute the leverages
    hTrain = leverage(xTrain)[0]
    hTest, hWarn = leverage(xTrain, xTest)

    print('Warning Leverage: ' + str(hWarn))
    adWarnTrain = hTrain > hWarn
    adWarnTest = hTest > hWarn

    header = ['Observed', 'Predicted', 'Residual', 'Normalized Residual', 'Leverage', 'Warning']
    dfTrain = pd.DataFrame(data=np.vstack((yTrain, yHatTrain, residualTrain, residualTrainNorm, hTrain, adWarnTrain)).T,
                           columns=header, index=dataTrain.index)
    dfTest = pd.DataFrame(data=np.vstack((yTest, yHatTest, residualTest, residualTestNorm, hTest, adWarnTest)).T,
                           columns=header, index=dataTest.index)

    dfTrain.to_csv(aDTrainOutputFile)
    dfTest.to_csv(aDTestOutputFile)

    obj.m_singleqsar_ad_residualTrainNorm = residualTrainNorm
    obj.m_singleqsar_ad_residualTestNorm  = residualTestNorm
    obj.m_singleqsar_ad_hTrain            = hTrain
    obj.m_singleqsar_ad_hTest             = hTest
    obj.m_singleqsar_ad_hWarn             = hWarn


def evoQSARyRand(obj):

    model = obj.m_singleqsar_model
    nRuns = obj.m_singleqsar_yrand_nruns

    model.yRand(int(nRuns))
    obj.yRandR2 = model.r2_yRand_
    obj.yRandQ2 = model.q2_yRand_

def evoQSARyRand_write(obj):

    outputFile = obj.m_singleqsar_yrand_outputfile

    header = ['R2', 'Q2']
    rows = zip(obj.yRandR2, obj.yRandQ2)

    with open(outputFile, 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for row in rows:
            writer.writerow(row)
    return True


def evoQSARPrediction(obj):
    model = obj.m_singleqsar_model
    xTrain = obj.m_training_dataset_x
    xPredict = obj.m_singleqsar_prediction_dataset_x
    dataPredict = obj.m_singleqsar_prediction_datapredict
    outputFile = obj.m_singleqsar_prediction_outputfile

    # Predict yHat of input dataset
    yHatPred = model.predict(xPredict)
    # Compute the leverages of predicted values
    hPred, hWarn = leverage(xTrain, xPredict)
    adWarnPred = hPred > hWarn
    header = ['Predicted', 'Leverage', 'Warning']

    dfPred = pd.DataFrame(data=np.vstack((yHatPred, hPred, adWarnPred)).T, columns=header, index=dataPredict.index)
    dfPred.to_csv(outputFile)




# Helper Functions

def leverage(xTrain, xQuery=None):
    """Calculates the leverage of training or query sample.
        Parameters:
    -----------
    xTrain : <NumPy array>, shape=(nSamples, nFeatures)
        * Data matrix used to compute the model.

    xQuery : <NumPy array> - Optional
        * Data matrix to compute the leverage.
        If empty, compute the leverage of the training data xTrain.

    Returns:
    --------
    h: <NumPy array>
        * Returns NumPy array of the leverage of the query sample.

    hWarn: <float>
        * Returns value of warning leverage.
        For a training set, h > hWarn may be highly influential data points.
        For prediction, h > hWarn indicates the predicted value is outside the model domain of applicability and
        requires extrapolation to make a prediction.
    """

    if xQuery is None:  # If no query sample, compute the leverage of training set.
        xQuery = xTrain

    hWarn = 3*(xTrain.shape[1] + 1)/xTrain.shape[0]    # Warning leverage

    trainInv = np.linalg.inv(xTrain.T.dot(xTrain))

    h = []
    for i in range(0, xQuery.shape[0]):
        h.append(xQuery[i, :].dot(trainInv).dot(xQuery[i, :].T))

    return np.array(h), hWarn

