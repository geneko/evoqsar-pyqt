""" Defines the main MainWindow class. Loads mainwindow.ui and other stuff...  """

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = ['Gene Ko']
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

from PyQt4 import QtGui, QtCore
import pandas as pd
import numpy as np
import webbrowser

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

import evoQSAR_GUI_V1.gui
from evoQSAR_GUI_V1 import evoqsartestscript
from evoQSAR_GUI_V1 import evoqsardbm

class evoQSARMainWindow(QtGui.QMainWindow):
    """ Main Window class for the evoQSAR filesystem application. """

    def __init__( self, parent = None ):
        super(evoQSARMainWindow, self).__init__(parent)

        # load the ui
        evoQSAR_GUI_V1.gui.loadUi( __file__, self )
        print ("\n*** GUI MainWindow *** ::\n__file__ = %s"  % (__file__))

        # Initialize DBM
        global dbm
        dbm = evoqsardbm.dbm()

        # Initialize GUI
        self.evoQSARMainWindowStartUp()

    def evoQSARMainWindowStartUp(self):

        print "evoQSARMainWindowStartUp"
        print dbm.__dict__

        # Initial Defines (DEBUG ONLY)
        # self.m_training_dataset = dbm.m_training_dataset
        # self.m_test_dataset = dbm.m_test_dataset
        # self.m_singleqsar_prediction_dataset = dbm.m_singleqsar_prediction_dataset


        # Initialize variables with default values from dbm
        self.maxTabIdx  = dbm.maxTabIdx
        self.figure  = dbm.figure
        self.layout_enabled  = dbm.layout_enabled


        # Prepopulate GUI lineedits with initial values
        self.loadPrePopulate()


        # Loads Tabs
        # print "evoQSARMainWindowStartUp -- enabled Tabs"
        self.loadTabs(dbm.enabledTabs)


        # Defines what's enabled
        self.loadEnabled()


        # Set textedit to read-only mode
        self.ui_singleqsar_statistics_txtedit.setReadOnly(True)


        # Ensure Combobox's are empty
        self.ui_modelingmethod_combobox.clear()
        self.ui_featureselection_combobox.clear()
        self.ui_cv_combobox.clear()
        self.ui_dataset_param_combobox.clear()

        # Populate Combobox's with values
        self.ui_modelingmethod_combobox.addItem("MLR")
        self.ui_modelingmethod_combobox.addItem("PLSR")
        self.ui_featureselection_combobox.addItem("BPSO")
        self.ui_featureselection_combobox.addItem("DE-BPSO")
        self.ui_cv_combobox.addItem("Leave one out")
        self.ui_cv_combobox.addItem("K-Fold")
        self.ui_dataset_param_combobox.addItem("None")
        self.ui_dataset_param_combobox.addItem("Standardize")
        self.ui_dataset_param_combobox.addItem("Rescale")


        # Populate Combobox's Default Values
        self.ui_dataset_param_combobox.setCurrentIndex(\
            self.ui_dataset_param_combobox.findText("None")) # Sets default item in dataset extra to 'None'
        self.ui_modelingmethod_combobox.setCurrentIndex(\
            self.ui_modelingmethod_combobox.findText('MLR'))  # Sets default item in modeling method combobox to MLR
        self.ui_featureselection_combobox.setCurrentIndex(\
            self.ui_featureselection_combobox.findText('DE-BPSO')) # Sets default item in feature selection combobox to DE-BPSO
        self.ui_cv_combobox.setCurrentIndex(\
            self.ui_cv_combobox.findText('Leave one out'))   # Sets default item in cross validation combobox to Leave One Out


        # Save Combobox selection on select
        self.ui_dataset_param_combobox.activated[str].connect( self.setDatasetExtra )
        self.ui_modelingmethod_combobox.activated[str].connect( self.setModelingMethod )
        self.ui_featureselection_combobox.activated[str].connect( self.setFeatureSelection )
        self.ui_cv_combobox.activated[str].connect( self.setCrossValidation )


    # Connections
        # Introduction
        self.ui_stepqsar_radiobtn.clicked.connect( self.setGuiMode )
        self.ui_singleqsar_radiobtn.clicked.connect( self.setGuiMode )


        # Datasets
        self.ui_trainingset_txtline.textChanged.connect( self.setTrainingSet )
        self.ui_validationset_txtline.textChanged.connect( self.setValidationSet )
        self.ui_testset_txtline.textChanged.connect( self.setTestSet )

        self.ui_trainingbrowse_btn.clicked.connect(self.selectTrainingFile)
        self.ui_validationbrowse_btn.clicked.connect(self.selectValidationFile)
        self.ui_testbrowse_btn.clicked.connect(self.selectTestFile)

        self.ui_datasetheader_chkbox.stateChanged.connect( self.setDatasetHeader )
        self.ui_datasetidxcol_chkbox.stateChanged.connect( self.setDatasetIdxCol )
        self.ui_datasetimport_btn.clicked.connect(self.importDataset)


        # Feature Selection
        self.ui_alpha_txtline_1.textChanged.connect( self.setAlpha1 )
        self.ui_alpha_txtline_2.textChanged.connect( self.setAlpha2 )
        self.ui_beta_txtline.textChanged.connect( self.setBeta )
        self.ui_f_txtline.textChanged.connect( self.setF )
        self.ui_cr_txtline.textChanged.connect( self.setCr)

        self.ui_initfeat_txtline.textChanged.connect( self.setInitFeat )
        self.ui_popsize_txtline.textChanged.connect( self.setPopSize )
        self.ui_maxgen_txtline.textChanged.connect( self.setMaxGen )
        self.ui_maxmodels_txtline.textChanged.connect( self.setMaxModels )
        self.ui_penaltyfactor_txtline.textChanged.connect( self.setPenaltyFactor )
        self.ui_parallel_chkbox.stateChanged.connect( self.setParallel )


        # Results
        self.ui_results_models_outputfile_txtedit.textChanged.connect(self.setModelsOutputFile)
        self.ui_results_importance_outputfile_txtedit.textChanged.connect(self.setImportanceOutputFile)
        self.ui_results_models_outputfile_btn.clicked.connect(self.selectModelsOutputFile)
        self.ui_results_importance_outputfile_btn.clicked.connect(self.selectImportanceOutputFile)
        self.ui_featureselection_results_init_btn.clicked.connect(self.modifyResultsTab)


        # Stepwise QSAR
        self.ui_testset_txtline_2.textChanged.connect( self.setTestSet2 )
        self.ui_datasetheader_chkbox_2.stateChanged.connect( self.setDatasetHeader2 )
        self.ui_datasetidxcol_chkbox_2.stateChanged.connect( self.setDatasetIdxCol2 )
        self.ui_datasetimport_btn_2.clicked.connect(self.importDataset2)
        self.ui_testbrowse_btn_2.clicked.connect(self.selectTestFile2)

        self.ui_maxdescript_txtline.textChanged.connect(self.setMaxNumDescriptors)
        self.ui_stepwise_outputfile_btn.clicked.connect(self.selectStepwiseOutputFile)
        self.ui_stepwise_outputfile_txtline.textChanged.connect(self.setStepwiseOutputFilename)


        # Correlation
        self.ui_singleqsar_correlation_init_btn.clicked.connect(self.modifyCorrelationTab)


        # Applicability Domain
        self.ui_singleqsar_ad_init_btn.clicked.connect(self.modifyADTab)
        self.ui_singleqsar_ad_trainoutputfile_btn.clicked.connect(self.selectADTrainOutputFile)
        self.ui_singleqsar_ad_testoutputfile_btn.clicked.connect(self.selectADTestOutputFile)
        self.ui_singleqsar_ad_trainoutputfile_txtline.textChanged.connect(self.setADTrainOutputFile)
        self.ui_singleqsar_ad_testoutputfile_txtline.textChanged.connect(self.setADTestOutputFile)
        self.ui_singleqsar_ad_run_btn.clicked.connect(self.runADScript)


        # y-Randomization
        self.ui_singleqsar_yrand_nruns_txtline.textChanged.connect(self.setYRandRuns)
        self.ui_singleqsar_yrand_run_btn.clicked.connect(self.runYRandScript)
        self.ui_singleqsar_yrand_init_btn.clicked.connect(self.modifyYRandomizationTab)
        self.ui_singleqsar_yrand_outputfile_btn.clicked.connect(self.selectYRandOutputFile)
        self.ui_singleqsar_yrand_outputfile_txtline.textChanged.connect(self.setYRandOutputFile)


        # Statistics
        self.ui_singleqsar_statistics_outputfile_txtline.textChanged.connect(self.setStatisticsOutputFile)
        self.ui_singleqsar_statistics_outputfile_btn.clicked.connect(self.selectStatisticsOutputFile)
        self.ui_singleqsar_statistics_run_btn.clicked.connect(self.outputStatistics)


        # Prediction
        self.ui_singleqsar_prediction_dataset_txtline.textChanged.connect(self.setPredictionSet)
        self.ui_singleqsar_prediction_browse_btn.clicked.connect(self.selectPredictionFile)
        self.ui_singleqsar_prediction_header_chkbox.stateChanged.connect( self.setPredictionHeader )
        self.ui_singleqsar_prediction_idxcol_chkbox.stateChanged.connect( self.setPredictionIdxCol )
        self.ui_singleqsar_prediction_outputfile_txtline.textChanged.connect(self.setPredictionOutputFile)
        self.ui_singleqsar_prediction_import_btn.clicked.connect(self.importPrediction_Dataset)
        self.ui_singleqsar_prediction_outputfile_btn.clicked.connect(self.selectPredictionOutputFile)
        self.ui_singleqsar_prediction_run_btn.clicked.connect(self.runPredictionScript)


        # Navigation
        self.ui_back_btn.clicked.connect(self.navigateBack)
        self.ui_next_btn.clicked.connect(self.navigateNext)


        # Run Buttons
        self.ui_run_btn.clicked.connect( self.runEvoQSARScript )
        self.ui_stepwise_run_btn.clicked.connect( self.runStepWiseScript )


        # Menu Actions
        self.ui_save_act.triggered.connect(self.saveObject)
        self.ui_saveas_act.triggered.connect(self.saveObject)
        self.ui_load_act.triggered.connect(self.loadObject)
        self.ui_quit_act.triggered.connect(self.fileExit)
        # self.ui_help_act
        self.ui_about_act.triggered.connect(self.helpAbout)

    def fileExit(self):
        self.close()    # Executes closeEvent()

    # Overrides main window closeEvent() function so a dialog box appears on application close.
    def closeEvent(self, event):
        msg = 'Are you sure you want to exit evoQSAR? Your changes will not be saved.'
        reply = QtGui.QMessageBox.question(self, 'Exit evoQSAR?',
                         msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def helpAbout(self):
        webbrowser.open('http://www.csrc.sdsu.edu/evoqsar')

    def setGuiMode(self):
        # Stepwise QSAR Mode
        if self.ui_stepqsar_radiobtn.isChecked():

            dbm.gui_mode = 'stepqsar'
            print dbm.gui_mode

            # Set Default Parameters
            dbm.m_n_components = 2
            self.ui_ncomponents_txtline.setText(str(dbm.m_n_components))

            # Enable tabs
            dbm.enabledTabs = [True, True, False, False, False, False, False, False, False, False, False]
            self.loadTabs(dbm.enabledTabs)

            # # Enable only Training and Validation dataset entry
            dbm.ui_bool_trainingbrowse_btn = True
            dbm.ui_bool_validationbrowse_btn = True
            dbm.ui_bool_testbrowse_btn = False
            dbm.ui_bool_trainingset_txtline = True
            dbm.ui_bool_validationset_txtline = True
            dbm.ui_bool_testset_txtline = False
            self.loadEnabled()


        # Single QSAR Mode
        if self.ui_singleqsar_radiobtn.isChecked():
            dbm.gui_mode = 'singleqsar'
            print dbm.gui_mode

            # Set Default Parameters
            dbm.m_n_components = 6
            self.m_n_components = dbm.m_n_components
            self.ui_ncomponents_txtline.setText(str(self.m_n_components))

            # Enable tabs
            dbm.enabledTabs = [True, True, False, False, False, False, False, False, False, False, False]
            self.loadTabs(dbm.enabledTabs)

            # # Enable only Training and Validation dataset entry
            dbm.ui_bool_trainingbrowse_btn = True
            dbm.ui_bool_validationbrowse_btn = False
            dbm.ui_bool_testbrowse_btn = True
            dbm.ui_bool_trainingset_txtline = True
            dbm.ui_bool_validationset_txtline = False
            dbm.ui_bool_testset_txtline = True
            self.loadEnabled()


# Navigation Buttons
    def navigateNext(self):
        # only enable next button if there is an enabled tab. If not then disable
        skipTabCount = 0
        currentTabIdx = self.ui_tab_widget.currentIndex()
        while (self.ui_tab_widget.isTabEnabled(currentTabIdx + 1) == False):
            skipTabCount += 1
            currentTabIdx += 1
            # Makes sure accessible tabs are within the total number of tabs available
            if currentTabIdx > self.maxTabIdx:
                break
        self.ui_tab_widget.setCurrentIndex(self.ui_tab_widget.currentIndex() + skipTabCount + 1)

    def navigateBack(self):
        # only enable back button if there is an enabled tab. If not then disable
        skipTabCount = 0
        currentTabIdx = self.ui_tab_widget.currentIndex()
        while (self.ui_tab_widget.isTabEnabled(currentTabIdx - 1) == False):
            skipTabCount -= 1
            currentTabIdx -= 1
            # Makes sure accessible tabs are within the total number of tabs available
            if currentTabIdx < 0:
                break
        self.ui_tab_widget.setCurrentIndex(self.ui_tab_widget.currentIndex() + skipTabCount - 1)


# Datasets
    def selectTrainingFile(self):
        self.ui_trainingset_txtline.setText(QtGui.QFileDialog.getOpenFileName(self, "Open - Training Set", "", 'Comma-Separated Values Files (*.csv)'))

    def setTrainingSet( self ):
        if self.ui_trainingset_txtline.text():
            dbm.m_training_dataset = str(self.ui_trainingset_txtline.text())
            dbm.m_training_dataset.replace(" ", "")
            dbm.ui_bool_trainingset_txtline = True
            self.loadEnabled()
            print ("\n*** m_training_set *** :: = %s"  % (dbm.m_training_dataset))

    def selectValidationFile(self):
        self.ui_validationset_txtline.setText(QtGui.QFileDialog.getOpenFileName(self, "Open - Validation Set", "", 'Comma-Separated Values Files (*.csv)'))

    def setValidationSet( self ):
        if self.ui_validationset_txtline.text():
            dbm.m_validation_dataset = str(self.ui_validationset_txtline.text())
            dbm.m_validation_dataset.replace(" ", "")
            dbm.ui_bool_validationset_txtline = True
            self.loadEnabled()
            print ("\n*** m_validation_set *** :: = %s"  % (dbm.m_validation_dataset))

    def selectTestFile(self):
        self.ui_testset_txtline.setText(QtGui.QFileDialog.getOpenFileName(self, "Open - Test Set", "", 'Comma-Separated Values Files (*.csv)'))

    def setTestSet( self ):
        if self.ui_testset_txtline.text():
            dbm.m_test_dataset = str(self.ui_testset_txtline.text())
            dbm.m_test_dataset.replace(" ", "")
            dbm.ui_bool_testset_txtline = True
            self.loadEnabled()
            print ("\n*** m_test_dataset*** :: = %s"  % (dbm.m_test_dataset))

    def setDatasetHeader( self, enabled ):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_dataset_header = value
        print ("\n***evoqsarmainwindow - setDatasetHeader***:: = %s" % (dbm.m_dataset_header))

    def setDatasetIdxCol( self, enabled ):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_dataset_idxcol = value
        print ("\n***evoqsarmainwindow - setDatasetIdxCol***:: = %s" % (dbm.m_dataset_idxcol))

    def setDatasetExtra(self, value):
        dbm.m_dataset_extra = value
        if dbm.m_dataset_extra  == "Standardize":
            self.setDatasetStd(2)
            self.setDatasetRescale(0)
        if dbm.m_dataset_extra  == "Rescale":
            self.setDatasetStd(0)
            self.setDatasetRescale(2)
        if dbm.m_dataset_extra  == "None":
            self.setDatasetStd(0)
            self.setDatasetRescale(0)

    def setDatasetStd( self, enabled ):
        if enabled == 2:
            value = True
        if enabled == 0:
            value = False
        dbm.m_dataset_std = value
        print ("\n***evoqsarmainwindow - setDatasetStd***:: = %s" % (dbm.m_dataset_std))

    def setDatasetRescale( self, enabled ):
        if enabled == 2:
            value = True
        if enabled == 0:
            value = False
        dbm.m_dataset_rescale = value
        print ("\n***evoqsarmainwindow - setDatasetRescale***:: = %s" % (dbm.m_dataset_rescale))

    def importDataset(self): # Returns a tuple (xTrain, yTrain, xValidation, yValidation, featureNames)
        if dbm.gui_mode == 'stepqsar':
            try:
                # Import data from csv into pandas
                dataTrain = pd.read_csv(dbm.m_training_dataset, header = dbm.m_dataset_header, index_col = dbm.m_dataset_idxcol)
                dataValidation = pd.read_csv(dbm.m_validation_dataset, header = dbm.m_dataset_header, index_col = dbm.m_dataset_idxcol)

                if dbm.m_dataset_header == 0:
                    featureNames = dataTrain.columns.values # <type 'numpy.ndarray'>
                    featureNamesFlag = True
                else:
                    featureNames = None
                    featureNamesFlag = False

                # Extract Activity Values
                # app.py = bpsorand.py || dataTrainX = xTrain   dataValidationX = xValidation
                yTrain = dataTrain.iloc[:, 0].values
                TrainX = dataTrain.iloc[:, 1:]
                yValidation = dataValidation.iloc[:, 0].values
                ValidationX = dataValidation.iloc[:, 1:]

                if dbm.m_dataset_std == True:
                    mean = TrainX.mean().values
                    std = TrainX.std().values
                    if dbm.m_dataset_std == True:
                        xTrain = ((TrainX - mean)/std).values
                        xValidation = ((ValidationX - mean)/std).values

                elif dbm.m_dataset_rescale == True:
                    min = TrainX.min().values
                    max = TrainX.max().values
                    if dbm.m_dataset_rescale == True:
                        xTrain = ((TrainX - min)/(max - min)).values
                        xValidation = ((ValidationX - min)/(max - min)).values

                else:
                    xTrain = TrainX.values
                    xValidation = ValidationX.values

                dbm.m_training_dataset_TrainX = TrainX
                dbm.m_training_dataset_x = xTrain
                dbm.m_training_dataset_y = yTrain
                dbm.m_validation_dataset_x = xValidation
                dbm.m_validation_dataset_y = yValidation
                dbm.m_featurenames = featureNames
                dbm.m_featurenamesflag = featureNamesFlag

                QtGui.QMessageBox.about(self, 'Success','Dataset has been successfully imported!')

                # Enable items after dataset is imported
                dbm.enabledTabs = [True, True, True, True, False, False, False, False, False, False, False]
                self.loadTabs(dbm.enabledTabs)

            except Exception:
                QtGui.QMessageBox.about(self, 'Failure','Dataset import has failed!')


        if dbm.gui_mode == 'singleqsar':
            try:
                # Import data from csv into pandas
                dataTrain = pd.read_csv(dbm.m_training_dataset, header = dbm.m_dataset_header, index_col = dbm.m_dataset_idxcol)
                dataTest = pd.read_csv(dbm.m_test_dataset, header = dbm.m_dataset_header, index_col = dbm.m_dataset_idxcol)

                if dbm.m_dataset_header == 0:
                    featureNames = dataTrain.columns.values # <type 'numpy.ndarray'>
                    featureNamesFlag = True
                else:
                    featureNames = None
                    featureNamesFlag = False

                # Extract Activity Values
                # app.py = bpsorand.py || dataTrainX = xTrain   dataValidationX = xValidation
                yTrain = dataTrain.iloc[:, 0].values
                TrainX = dataTrain.iloc[:, 1:]
                yTest = dataTest.iloc[:, 0].values
                TestX = dataTest.iloc[:, 1:]

                if dbm.m_dataset_std == True:
                    mean = TrainX.mean().values
                    std = TrainX.std().values
                    if dbm.m_dataset_std == True:
                        xTrain = ((TrainX - mean)/std).values
                        xTest = ((TestX - mean)/std).values

                elif dbm.m_dataset_rescale == True:
                    min = TrainX.min().values
                    max = TrainX.max().values
                    if dbm.m_dataset_rescale == True:
                        xTrain = ((TrainX - min)/(max - min)).values
                        xTest = ((TestX - min)/(max - min)).values

                else:
                    xTrain = TrainX.values
                    xTest = TestX.values

                dbm.m_training_dataset_TrainX = TrainX # dataTrain
                dbm.m_test_dataset_TestX = TestX # dataTest
                dbm.m_training_dataset_x = xTrain
                dbm.m_training_dataset_y = yTrain
                dbm.m_test_dataset_x = xTest
                dbm.m_test_dataset_y = yTest
                dbm.m_featurenames = featureNames
                dbm.m_featurenamesflag = featureNamesFlag

                QtGui.QMessageBox.about(self, 'Success','Dataset has been successfully imported!')

                # Enable Tabs after dataset is imported
                dbm.enabledTabs = [True, True, False, True, False, False, False, False, False, False, False]
                self.loadTabs(dbm.enabledTabs)

            except Exception:
                QtGui.QMessageBox.about(self, 'Failure','Dataset import has failed!')


# Feature Selection
    def setFeatureSelection(self, value):
        if value == "BPSO":
            dbm.ui_bool_alpha_txtline_1 = True
            dbm.ui_bool_alpha_txtline_2 = True
            dbm.ui_bool_beta_txtline    = True
            dbm.ui_bool_f_txtline       = False
            dbm.ui_bool_cr_txtline      = False

        elif value == "DE-BPSO":
            dbm.ui_bool_alpha_txtline_1 = True
            dbm.ui_bool_alpha_txtline_2 = True
            dbm.ui_bool_beta_txtline    = True
            dbm.ui_bool_f_txtline       = True
            dbm.ui_bool_cr_txtline      = True

        else:
            dbm.ui_bool_alpha_txtline_1 = False
            dbm.ui_bool_alpha_txtline_2 = False
            dbm.ui_bool_beta_txtline    = False
            dbm.ui_bool_f_txtline       = False
            dbm.ui_bool_cr_txtline      = False

        self.loadEnabled()
        dbm.m_feature_selection = str(value)
        print ("\n*** m_feature_selection *** :: = %s"  % (dbm.m_feature_selection))

    def setAlpha1( self ):
        if self.ui_alpha_txtline_1.text():
            try:
                dbm.m_alpha1_param = float(self.ui_alpha_txtline_1.text())
                print ("\n*** m_alpha1_param *** :: = %s"  % (dbm.m_alpha1_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an float')
                pass

            if dbm.m_alpha1_param < 0 or dbm.m_alpha1_param > 1:
                QtGui.QMessageBox.about(self, 'Error','Alpha 1 must be within the following ranges: ( 0, 1 )')
                dbm.m_alpha1_param = 0.33
            pass

    def setAlpha2( self ):
        if self.ui_alpha_txtline_2.text():
            try:
                dbm.m_alpha2_param = float(self.ui_alpha_txtline_2.text())
                print ("\n*** m_alpha2_param *** :: = %s"  % (dbm.m_alpha2_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an float')
                pass

            if dbm.m_alpha2_param < 0 or dbm.m_alpha2_param > 1:
                QtGui.QMessageBox.about(self, 'Error','Alpha 1 must be within the following ranges: ( 0, 1 )')
                dbm.m_alpha2_param = 0.5
            pass


    def setBeta( self ):
        if self.ui_beta_txtline.text():
            try:
                dbm.m_beta_param = float(self.ui_beta_txtline.text())
                print ("\n*** m_beta_param *** :: = %s"  % (dbm.m_beta_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an float')
                pass

            if dbm.m_beta_param < 0:
                QtGui.QMessageBox.about(self, 'Error','Beta must be within the following ranges: ( 0, # Features )')
                dbm.m_beta_param = 1.5
            pass

    def setF( self ):
        if self.ui_f_txtline.text():
            try:
                dbm.m_f_param = float(self.ui_f_txtline.text())
                print ("\n*** m_f_param *** :: = %s"  % (dbm.m_f_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an float')
                pass

            if dbm.m_f_param < 0 or dbm.m_f_param > 2:
                QtGui.QMessageBox.about(self, 'Error','F must be within the following ranges: ( 0, 2 )')
                dbm.m_f_param = 0.8
            pass

    def setCr( self ):
        if self.ui_cr_txtline.text():
            try:
                dbm.m_cr_param = float(self.ui_cr_txtline.text())
                print ("\n*** m_cr_param *** :: = %s"  % (dbm.m_cr_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an float')
                pass

            if dbm.m_cr_param < 0 or dbm.m_cr_param > 1:
                QtGui.QMessageBox.about(self, 'Error','CR must be within the following ranges: ( 0, 1 )')
                dbm.m_cr_param = 0.8
            pass

    def setInitFeat( self ):
        if self.ui_initfeat_txtline.text():
            try:
                dbm.m_initfeat_param = int(self.ui_initfeat_txtline.text())
                print ("\n*** m_initfeat_param *** :: = %s"  % (dbm.m_initfeat_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an integer')
                pass

    def setPopSize( self ):
        if self.ui_popsize_txtline.text():
            try:
                dbm.m_popsize_param = int(self.ui_popsize_txtline.text())
                print ("\n*** m_popsize_param *** :: = %s"  % (dbm.m_popsize_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an integer')
                pass

            if dbm.m_popsize_param < 1:
                QtGui.QMessageBox.about(self, 'Error','popSize must be greater than 0')
                dbm.m_popsize_param = 20
                print ("\n*** m_popsize_param *** :: = %s"  % (dbm.m_popsize_param))
            pass

    def setMaxGen( self ):
        if self.ui_maxgen_txtline.text():
            try:
                dbm.m_maxgen_param = int(self.ui_maxgen_txtline.text())
                print ("\n*** m_maxgen_param *** :: = %s"  % (dbm.m_maxgen_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an integer')
                pass

            if dbm.m_maxgen_param < 1:
                QtGui.QMessageBox.about(self, 'Error','maxGen must be greater than 0')
                dbm.m_maxgen_param = 2000
                print ("\n*** m_maxgen_param *** :: = %s"  % (dbm.m_maxgen_param))
            pass

    def setMaxModels( self ):
        if self.ui_maxmodels_txtline.text():
            try:
                dbm.m_maxmodels_param = int(self.ui_maxmodels_txtline.text())
                print ("\n*** m_maxmodels_param *** :: = %s"  % (dbm.m_maxmodels_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an integer')
                pass

            if dbm.m_maxmodels_param < 1:
                QtGui.QMessageBox.about(self, 'Error','maxModels must be greater than 0')
                dbm.m_maxmodels_param = 100
                print ("\n*** m_maxmodels_param *** :: = %s"  % (dbm.m_maxmodels_param))
            pass

    def setPenaltyFactor( self ):
        if self.ui_penaltyfactor_txtline.text():
            try:
                dbm.m_penaltyfactor_param = int(self.ui_penaltyfactor_txtline.text())
                print ("\n*** m_penaltyfactor_param *** :: = %s"  % (dbm.m_penaltyfactor_param))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an integer')
                pass

            if dbm.m_penaltyfactor_param < 0 or dbm.m_penaltyfactor > 100:
                QtGui.QMessageBox.about(self, 'Error','Penalty Factor must be within the following ranges: ( 0, 100 )')
                dbm.m_penaltyfactor_param = 2
                print ("\n*** m_penaltyfactor_param *** :: = %s"  % (dbm.m_penaltyfactor_param))
            pass

    def setParallel( self, enabled ):
        if enabled == 2:
            value = -1 # utilize all processors
        if enabled == 0:
            value = 1 # utilize only 1 processor
        dbm.m_parallel_param = value
        print ("\n***setParallel***:: = %s" % (dbm.m_parallel_param))


# Modeling
    def setModelingMethod(self, value):
        if value == "PLSR":
            dbm.ui_bool_ncomponents_txtline = True
            dbm.ui_bool_cv_nfold_txtline    = False
        else:
            dbm.ui_bool_ncomponents_txtline = False
            dbm.ui_bool_cv_nfold_txtline    = False

        self.loadEnabled()
        self.ui_ncomponents_txtline.textChanged.connect( self.setNComponents )
        dbm.m_modeling_method = str(value)
        print ("\n*** m_modeling_method *** :: = %s"  % (dbm.m_modeling_method))

    def setNComponents(self):
        if self.ui_ncomponents_txtline.text():
            try:
                dbm.m_n_components = int(self.ui_ncomponents_txtline.text())
                print ("\n*** m_n_components *** :: = %i" % (dbm.m_n_components))
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an int')
                pass

            if dbm.m_n_components < 2:
                QtGui.QMessageBox.about(self, 'Error','nComponents must be within the following ranges: [ 2, # Features ]')
                dbm.m_n_components = 2
                print ("\n*** m_n_components *** :: = %i" % (dbm.m_n_components))
            pass

    def setCrossValidation(self, value):
        dbm.m_paramcv_value = value
        if dbm.m_paramcv_value == "Leave one out":
            dbm.m_paramcv_dict['nFold'] = -1
            for key, value in self.m_paramcv_dict.iteritems() :
                print key, value
            dbm.ui_bool_cv_nfold_txtline          = False
            dbm.ui_bool_cv_shuffle_chkbox         = False
            dbm.ui_bool_cv_shuffle_nruns_txtline  = False

        elif dbm.m_paramcv_value == "K-Fold":
            dbm.ui_bool_cv_nfold_txtline          = True
            dbm.ui_bool_cv_shuffle_chkbox         = True
            self.ui_cv_nfold_txtline.editingFinished.connect( self.setNFold )
            self.ui_cv_shuffle_chkbox.stateChanged.connect( self.setShuffle )
            if self.ui_cv_shuffle_chkbox.isChecked():
                dbm.ui_bool_cv_shuffle_nruns_txtline  = False
                self.loadEnabled()
        else:
            dbm.ui_bool_cv_nfold_txtline          = False
            dbm.ui_bool_cv_shuffle_chkbox         = False

        self.loadEnabled()

    def setNFold(self):
        if self.ui_cv_nfold_txtline.text():
            try:
                dbm.m_cv_nfold_param = int(self.ui_cv_nfold_txtline.text())
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an int')
                pass

            if dbm.m_cv_nfold_param < 2:
                QtGui.QMessageBox.about(self, 'Error','nFold must be within the following ranges: [ 2, # of Rows in Training Set ] if K-Fold is selected')
                dbm.m_cv_nfold_param = 2
            pass

            dbm.m_paramcv_dict['nFold'] = dbm.m_cv_nfold_param
            for key, value in dbm.m_paramcv_dict.iteritems() :
                print key, value

    def setShuffle( self, enabled ):
        if enabled == 2:
            value = 0
            dbm.ui_bool_cv_shuffle_nruns_txtline  = True
            self.loadEnabled()
            dbm.m_paramcv_dict['shuffle'] = True
            self.ui_cv_shuffle_nruns_txtline.editingFinished.connect( self.setNRuns )
            for key, value in dbm.m_paramcv_dict.iteritems() :
                print key, value

        if enabled == 0:
            value = None
            dbm.ui_bool_cv_shuffle_nruns_txtline  = False
            self.loadEnabled()
            if 'shuffle' in dbm.m_paramcv_dict.keys():
                del dbm.m_paramcv_dict['shuffle']
            if 'nRuns' in dbm.m_paramcv_dict:
                del dbm.m_paramcv_dict['nRuns']
            for key, value in dbm.m_paramcv_dict.iteritems() :
                print key, value
        dbm.m_cv_shuffle_chkbox = value

    def setNRuns(self):
        if self.ui_cv_shuffle_nruns_txtline.text():
            try:
                dbm.m_cv_shuffle_nruns_param = int(self.ui_cv_shuffle_nruns_txtline.text())
            except Exception:
                QtGui.QMessageBox.about(self, 'Error','Input can only be an int')
                pass

            if dbm.m_cv_shuffle_nruns_param < 1:
                QtGui.QMessageBox.about(self, 'Error','nRuns must be within the following ranges: [ 1, infinity ] if shuffle is enabled')
                dbm.m_cv_shuffle_nruns_param = 2
            pass

            dbm.m_paramcv_dict['nRuns'] = dbm.m_cv_shuffle_nruns_param
            for key, value in dbm.m_paramcv_dict.iteritems() :
                print key, value

    def runEvoQSARScript( self ):

        if dbm.gui_mode == 'stepqsar':
            try:
                QtGui.QMessageBox.about(self, 'Run','Simulation has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')

                if evoqsartestscript.evoQSARTestScript(dbm):
                    QtGui.QMessageBox.about(self, 'Success','Your run was successful!')
                else:
                    QtGui.QMessageBox.about(self, 'Error','An Error occured while running evoQSARTestScript')

                # for testing only
                # self.m_featFreq = [(191, 79), (24, 79), (6, 79), (244, 79), (25, 79), (174, 78), (130, 46), (348, 24), (122, 22), (175, 11), (319, 11), (158, 10), (365, 10), (80, 9), (239, 8), (19, 8), (320, 7), (318, 6), (373, 5), (11, 4), (377, 4), (173, 3), (159, 3), (14, 3), (23, 3), (89, 3), (213, 3), (88, 2), (137, 2), (350, 2), (134, 2), (144, 2), (356, 2), (83, 2), (140, 2), (166, 2), (49, 2), (262, 2), (12, 2), (156, 2), (168, 2), (110, 2), (242, 2), (240, 2), (160, 2), (372, 2), (82, 1), (259, 1), (347, 1), (132, 1), (157, 1), (171, 1), (253, 1), (382, 1), (169, 1), (86, 1), (375, 1), (124, 1), (53, 1), (261, 1), (282, 1), (96, 1), (222, 1), (249, 1), (176, 1), (127, 1), (125, 1), (241, 1), (263, 1), (202, 1), (360, 1), (367, 1), (342, 1), (232, 1), (354, 1), (43, 1), (45, 1), (151, 1)]

                if not dbm.m_featFreq: # If no models were found... self.m_featFreq should be 'None'
                    QtGui.QMessageBox.about(self, 'No Models Found', 'Sorry, no models were found... Please try again.')
                else:
                    if len(dbm.m_featFreq) < 10:
                        QtGui.QMessageBox.about(self, 'Error', 'Not enough models were found... Please try again.')
                    else:
                        QtGui.QMessageBox.about(self, 'Models Found!!!', 'Models Found! Please proceed to the Results and Stepwise QSAR tabs')

                        # Enable tabs
                        dbm.enabledTabs = [True, True, True, True, True, True, False, False, False, False, False]
                        self.loadTabs(dbm.enabledTabs)

            except Exception:
                QtGui.QMessageBox.about(self, 'Failure','A failure occurred, please re-check your values and run again.')

        if dbm.gui_mode == 'singleqsar':
            try:
                QtGui.QMessageBox.about(self, 'Run','Single QSAR has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
                if evoqsartestscript.evoQSARSingle(dbm):
                    QtGui.QMessageBox.about(self, 'Success','Your run was successful!')

                    # Enable tabs
                    dbm.enabledTabs = [True, True, False, True, False, False, True, True, True, True, True]
                    self.loadTabs(dbm.enabledTabs)

                else:
                    QtGui.QMessageBox.about(self, 'Error','An Error occured while running Single QSAR')
            except Exception:
                    QtGui.QMessageBox.about(self, 'Failure','A failure occurred, please re-check your values and run again.')


# Results
    def selectModelsOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Predictive Models", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_results_models_outputfile_txtedit.setText(filename)

        dbm.ui_bool_results_models_outputfile_txtedit = True
        self.loadEnabled()

    def setModelsOutputFile(self):
        if self.ui_results_models_outputfile_txtedit.text():
            dbm.m_results_models_outputfile = str(self.ui_results_models_outputfile_txtedit.text())
            dbm.m_results_models_outputfile.replace(" ", "")
            print ("\n*** setModelsOutputFile*** :: = %s"  % (dbm.m_results_models_outputfile))
            QtGui.QMessageBox.about(self, 'Run','Models calculations has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
            evoqsartestscript.evoQSAROutputModels(dbm)
            QtGui.QMessageBox.about(self, 'Success','Your calculation was successful!')

    def selectImportanceOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Feature Importance", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_results_importance_outputfile_txtedit.setText(filename)

        dbm.ui_bool_results_importance_outputfile_txtedit = True
        self.loadEnabled()

    def setImportanceOutputFile(self):
        if self.ui_results_importance_outputfile_txtedit.text():
            dbm.m_results_importance_outputfile = str(self.ui_results_importance_outputfile_txtedit.text())
            dbm.m_results_importance_outputfile.replace(" ", "")
            print ("\n*** setImportanceOutputFile*** :: = %s"  % (dbm.m_results_importance_outputfile))
            QtGui.QMessageBox.about(self, 'Run','Importance calculations has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
            evoqsartestscript.evoQSAROutputImportance(dbm)
            QtGui.QMessageBox.about(self, 'Success','Your calculation was successful!')

    def modifyResultsTab(self):
        # If plot has already been initialized, clear everything first before rerunning
        if self.layout_enabled:
            self.figure.clf()
            self.layout.removeWidget(self.toolbar)
            self.layout.removeWidget(self.canvas)
            self.layout.removeWidget(self.button)

        # a figure instance to plot on
        self.figure = plt.figure(1)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget as a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot_results)

        # set the layout
        self.layout = self.ui_mplvl_results_layout
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.button)
        self.layout_enabled = True

    def plot_results(self):
        if dbm.m_testscript_flag == False:
            QtGui.QMessageBox.question(QtGui.QMessageBox, 'Error',
            "The simulation has not finished yet!\nPlease wait...", QtGui.QMessageBox.Ok,
            QtGui.QMessageBox.Ok)
        else:
            unzipped = zip(*dbm.m_featFreq)
            dbm.m_featureid = list(unzipped[0])[:20]
            dbm.m_featurefrequency = list(unzipped[1])[:20]
            dbm.m_featureidname = []
            if dbm.m_featurenamesflag == True:
                for featureid in dbm.m_featureid:
                    dbm.m_featureidname.append(dbm.m_featurenames[featureid])
                    x_values = list(reversed(dbm.m_featureidname))
            else:
                x_values = dbm.m_featureid

        y_pos = np.arange(len(x_values))
        y_values = list(reversed(dbm.m_featurefrequency))

        self.figure.clear()
        plt.barh(y_pos, y_values, align='center')
        plt.yticks(y_pos, x_values)
        plt.xlabel('Frequency')
        plt.ylabel('Features')
        plt.title('Frequency of Top Ranked Features')
        plt.tight_layout()

        # refresh canvas
        self.canvas.draw()

        # disable future plotting until re-initialized
        self.button.setEnabled(False)


# Stepwise QSAR
    def selectTestFile2(self):
        self.ui_testset_txtline_2.setText(QtGui.QFileDialog.getOpenFileName(self, "Open - Test Set", "", 'Comma-Separated Values Files (*.csv)'))

    def setTestSet2( self ):
        if self.ui_testset_txtline_2.text():
            dbm.m_test_dataset_2 = str(self.ui_testset_txtline_2.text())
            dbm.m_test_dataset_2.replace(" ", "")
            dbm.ui_bool_testset_txtline_2 = True
            self.loadEnabled()
            print ("\n*** m_test_dataset_2*** :: = %s"  % (dbm.m_test_dataset_2))

    def setDatasetHeader2( self, enabled ):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_dataset_header_2 = value
        print ("\n***evoqsarmainwindow - setDatasetHeader2***:: = %s" % (dbm.m_dataset_header_2))

    def setDatasetIdxCol2( self, enabled ):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_dataset_idxcol_2 = value
        print ("\n***evoqsarmainwindow - setDatasetIdxCol2***:: = %s" % (dbm.m_dataset_idxcol_2))

    def importDataset2(self): # For Stepwise QSAR -- Returns a tuple (xTrain, yTrain, xValidation, yValidation, featureNames)
        try:
            # Import other variables
            TrainX = dbm.m_training_dataset_TrainX

            # Import data from csv into pandas
            dataTest = pd.read_csv(dbm.m_test_dataset_2, header = dbm.m_dataset_header_2, index_col = dbm.m_dataset_idxcol_2)

            # Extract Activity Values
            # app.py = bpsorand.py || dataTrainX = xTrain   dataValidationX = xValidation
            yTest = dataTest.iloc[:, 0].values
            TestX = dataTest.iloc[:, 1:]

            if dbm.m_dataset_std == True:
                mean = TrainX.mean().values
                std = TrainX.std().values
                if dbm.m_dataset_std == True:
                    xTest = ((TestX - mean)/std).values

            elif dbm.m_dataset_rescale == True:
                min = TrainX.min().values
                max = TrainX.max().values
                if dbm.m_dataset_rescale == True:
                    xTest = ((TestX - min)/(max - min)).values

            else:
                xTest = TestX.values

            dbm.m_test_dataset_2_x = xTest
            dbm.m_test_dataset_2_y = yTest

            # Enable Run button after dataset is imported
            dbm.ui_bool_stepwise_run_btn = True
            self.loadEnabled()
            QtGui.QMessageBox.about(self, 'Success','Dataset has been successfully imported!')

        except Exception:
             QtGui.QMessageBox.about(self, 'Failure','Dataset import has failed!')


    def setMaxNumDescriptors(self):
        if self.ui_maxdescript_txtline.text():
            dbm.m_max_descript = str(self.ui_maxdescript_txtline.text())
            dbm.m_max_descript.replace(" ", "")
            if dbm.m_modeling_method == "PLSR":
                if dbm.m_max_descript <= dbm.m_n_components:
                    QtGui.QMessageBox.about(self, 'Error','Max Descriptors has to be greater than n_components\n Defaulting to 20')
                    dbm.m_max_descript = 20
            if dbm.m_modeling_method == "MLR":
                if dbm.m_max_descript <= 2:
                    QtGui.QMessageBox.about(self, 'Error','Max Descriptors has to be greater than 2\n Defaulting to 20')
                    dbm.m_max_descript = 20
            print ("\n*** m_max_descript*** :: = %s"  % (dbm.m_max_descript))

    def selectStepwiseOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Stepwise Modeling", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_stepwise_outputfile_txtline.setText(filename)

    def setStepwiseOutputFilename(self):
        if self.ui_stepwise_outputfile_txtline.text():
            dbm.m_stepwise_outputfile = str(self.ui_stepwise_outputfile_txtline.text())
            dbm.m_stepwise_outputfile.replace(" ", "")
            dbm.ui_bool_stepwise_outputfile_txtline = True
            self.loadEnabled()

            print ("\n*** m_stepwise_outputfile*** :: = %s"  % (dbm.m_stepwise_outputfile))

    def runStepWiseScript(self):
        try:
            QtGui.QMessageBox.about(self, 'Run','StepWise QSAR has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
            evoqsartestscript.evoQSARStepWise(dbm)
            QtGui.QMessageBox.about(self, 'Success','Your run was successful!')
        except Exception:
            QtGui.QMessageBox.about(self, 'Failure','A failure occurred, please re-check your values and run again.')


# Correlation
    def modifyCorrelationTab(self):

        # If plot has already been initialized, clear everything first before rerunning
        if self.layout_enabled:
            self.figure.clf()
            self.layout.removeWidget(self.toolbar)
            self.layout.removeWidget(self.canvas)
            self.layout.removeWidget(self.button)

        # a figure instance to plot on
        self.figure = plt.figure(2)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget as a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot_correlation)

        # set the layout
        self.layout = self.ui_mplvl_correlation_layout
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.button)
        self.layout_enabled = True

    def plot_correlation(self):

        yTrain = dbm.m_training_dataset_y
        yHatTrain = dbm.m_singleqsar_yHatTrain
        yTest = dbm.m_test_dataset_y
        yHatTest = dbm.m_singleqsar_yHatTest

        y = np.concatenate((yTrain, yHatTrain,yTest, yHatTest))
        yMin = np.floor(y.min())
        yMax = np.ceil(y.max())

        plt.plot(yTrain, yHatTrain, 'r.')
        plt.plot(yTest, yHatTest, 'b.')
        plt.axis([yMin, yMax, yMin, yMax])
        plt.xlabel('Observed')
        plt.ylabel('Predicted')
        plt.legend(['Training', 'Test'])
        plt.plot([yMin, yMax], [yMin, yMax], 'k-')
        plt.tight_layout()

        # refresh canvas
        self.canvas.draw()

        # disable future plotting until re-initialized
        self.button.setEnabled(False)


# Applicability Domain
    def selectADTrainOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Applicability Domain (Training Set)", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_singleqsar_ad_trainoutputfile_txtline.setText(filename)

    def selectADTestOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Applicability Domain (Test Set)", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_singleqsar_ad_testoutputfile_txtline.setText(filename)

    def setADTrainOutputFile(self):
        if self.ui_singleqsar_ad_trainoutputfile_txtline.text():
            dbm.m_singleqsar_ad_trainoutputfile = str(self.ui_singleqsar_ad_trainoutputfile_txtline.text())
            dbm.m_singleqsar_ad_trainoutputfile.replace(" ", "")
            dbm.ui_bool_singleqsar_ad_trainoutputfile_txtline = True
            self.loadEnabled()
            print ("\n*** setADTrainOutputFile*** :: = %s"  % (dbm.m_singleqsar_ad_trainoutputfile))

    def setADTestOutputFile(self):
        if self.ui_singleqsar_ad_testoutputfile_txtline.text():
            dbm.m_singleqsar_ad_testoutputfile = str(self.ui_singleqsar_ad_testoutputfile_txtline.text())
            dbm.m_singleqsar_ad_testoutputfile.replace(" ", "")
            dbm.ui_bool_singleqsar_ad_testoutputfile_txtline = True
            self.loadEnabled()
            print ("\n*** setADTestOutputFile*** :: = %s"  % (dbm.m_singleqsar_ad_testoutputfile))

    def runADScript(self):
        QtGui.QMessageBox.about(self, 'Run','Applicability Domain calculations has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
        evoqsartestscript.evoQSARAD(dbm)
        QtGui.QMessageBox.about(self, 'Success','Your calculation was successful!')
        dbm.ui_bool_singleqsar_ad_init_btn = True
        self.loadEnabled()

    def modifyADTab(self):

        # If plot has already been initialized, clear everything first before rerunning
        if self.layout_enabled:
            self.figure.clf()
            self.layout.removeWidget(self.toolbar)
            self.layout.removeWidget(self.canvas)
            self.layout.removeWidget(self.button)

        # a figure instance to plot on
        self.figure = plt.figure(3)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot_ad)

        # set the layout
        self.layout = self.ui_mplvl_ad_layout
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.button)
        self.layout_enabled = True

    def plot_ad(self):

        residualTrainNorm = dbm.m_singleqsar_ad_residualTrainNorm
        residualTestNorm= dbm.m_singleqsar_ad_residualTestNorm
        hTrain = dbm.m_singleqsar_ad_hTrain
        hTest = dbm.m_singleqsar_ad_hTest
        hWarn = dbm.m_singleqsar_ad_hWarn

        # Plot Leverages
        # Determine axis limits
        y = np.concatenate((residualTrainNorm, residualTestNorm))
        x = np.concatenate((hTrain, hTest))
        yMin = np.floor(y.min())
        yMax = np.ceil(y.max())
        xMax = np.ceil(x.max())
        if yMin > -3:
            yMin = -3.5
        if yMax < 3:
            yMax = 3.5
        if xMax < hWarn:
            xMax = np.ceil(hWarn)

        plt.plot(hTrain, residualTrainNorm, 'r.')
        plt.plot(hTest, residualTestNorm, 'g.')
        plt.plot([0, xMax], [3, 3], 'k--')
        plt.plot([0, xMax], [-3, -3], 'k--')
        plt.plot([hWarn, hWarn], [yMin, yMax], 'k--')
        plt.axis([0, xMax, yMin, yMax])
        title = 'Williams Plot (Warning Leverage: ' + str(hWarn) + ')'
        plt.title(title)
        plt.xlabel('Leverage')
        plt.ylabel('Normalized Residual')
        plt.legend(['Training', 'Test'])
        plt.tight_layout()

        # refresh canvas
        self.canvas.draw()

        # disable future plotting until re-initialized
        self.button.setEnabled(False)


# y-Randomization
    def setYRandRuns(self):
        if self.ui_singleqsar_yrand_nruns_txtline.text():
            dbm.m_singleqsar_yrand_nruns = str(self.ui_singleqsar_yrand_nruns_txtline.text())
            dbm.m_singleqsar_yrand_nruns.replace(" ", "")
            print ("\n*** setYRandRuns*** :: = %s"  % (dbm.m_singleqsar_yrand_nruns))

    def runYRandScript(self):
        QtGui.QMessageBox.about(self, 'Run','y-Rand calculations has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
        evoqsartestscript.evoQSARyRand(dbm)
        QtGui.QMessageBox.about(self, 'Success','Your calculation was successful!')
        dbm.ui_bool_singleqsar_yrand_outputfile_btn = True
        dbm.ui_bool_singleqsar_yrand_init_btn = True
        self.loadEnabled()


    def selectYRandOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - y-Randomization", "", 'Comma-Separated Values Files (*.csv)')
        dbm.m_yRand_user_input = True
        self.ui_singleqsar_yrand_outputfile_txtline.setText(filename)
        dbm.ui_bool_singleqsar_yrand_outputfile_txtline = True
        self.loadEnabled()


    def setYRandOutputFile(self):
        if self.ui_singleqsar_yrand_outputfile_txtline.text():
            dbm.m_singleqsar_yrand_outputfile = str(self.ui_singleqsar_yrand_outputfile_txtline.text())
            dbm.m_singleqsar_yrand_outputfile.replace(" ", "")
            if dbm.m_yRand_user_input == True:
                if evoqsartestscript.evoQSARyRand_write(dbm):
                    QtGui.QMessageBox.about(self, 'Success','y-Rand statistics were outputted successfully!')
                    dbm.m_yRand_user_input == False
        print ("\n*** setyRandOutputFile*** :: = %s"  % (dbm.m_singleqsar_yrand_outputfile))

    def modifyYRandomizationTab(self):
        # If plot has already been initialized, clear everything first before rerunning
        if self.layout_enabled:
            self.figure.clf()
            self.layout.removeWidget(self.toolbar)
            self.layout.removeWidget(self.canvas)
            self.layout.removeWidget(self.button)

        # a figure instance to plot on
        self.figure = plt.figure(4)

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot_yrandomization)

        # set the layout
        self.layout = self.ui_mplvl_yrand_layout
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.button)
        self.layout_enabled = True

    def plot_yrandomization(self):
        plt.plot(dbm.yRandR2, dbm.yRandQ2, 'k.')
        plt.axis([-1, 1, -1, 1])
        plt.title('y-Randomization')
        plt.xlabel(r'R$^2$')
        plt.ylabel(r'Q$^2$')
        plt.tight_layout()

        # refresh canvas
        self.canvas.draw()

        # disable future plotting until re-initialized
        self.button.setEnabled(False)

# Statistics
    def selectStatisticsOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Model Statistics", "", 'Text Files (*.txt)')
        self.ui_singleqsar_statistics_outputfile_txtline.setText(filename)
        dbm.ui_bool_singleqsar_statistics_outputfile_txtline = True
        self.loadEnabled()


    def setStatisticsOutputFile(self):
        if self.ui_singleqsar_statistics_outputfile_txtline.text():
            dbm.m_singleqsar_statistics_outputfile = str(self.ui_singleqsar_statistics_outputfile_txtline.text())
            dbm.m_singleqsar_statistics_outputfile.replace(" ", "")
        print ("\n*** setStatisticsOutputFile*** :: = %s"  % (dbm.m_singleqsar_statistics_outputfile))

    def outputStatistics(self):
        try:
            # Output Statistics to File
            outputFile = dbm.m_singleqsar_statistics_outputfile
            output = dbm.m_singleqsar_outputStats
            f = open(outputFile,'w')
            f.write(output)
            f.close()
            # Output to GUI
            self.ui_singleqsar_statistics_txtedit.setText(dbm.m_singleqsar_outputStats)
            QtGui.QMessageBox.about(self, 'Success','Statistics were outputted successfully!')
        except Exception:
            QtGui.QMessageBox.about(self, 'Failure','A failure occurred, please re-check your values and run again.')


# Prediction
    def selectPredictionFile(self):
       self.ui_singleqsar_prediction_dataset_txtline.setText(QtGui.QFileDialog.getOpenFileName(self, "Open - Prediction Set", "", 'Comma-Separated Values Files (*.csv)'))

    def setPredictionSet(self):
        if self.ui_singleqsar_prediction_dataset_txtline.text():
            dbm.m_singleqsar_prediction_dataset = str(self.ui_singleqsar_prediction_dataset_txtline.text())
            dbm.m_singleqsar_prediction_dataset.replace(" ", "")
            dbm.ui_bool_singleqsar_prediction_dataset_txtline = True
            self.loadEnabled()
            print ("\n*** m_singleqsar_prediction_dataset*** :: = %s"  % (dbm.m_singleqsar_prediction_dataset))

    def setPredictionHeader(self, enabled):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_singleqsar_prediction_header = value
        print ("\n***evoqsarmainwindow - setPredictionHeader***:: = %s" % (dbm.m_singleqsar_prediction_header))

    def setPredictionIdxCol(self, enabled):
        if enabled == 2:
            value = 0
        if enabled == 0:
            value = None
        dbm.m_singleqsar_prediction_IdxCol = value
        print ("\n***evoqsarmainwindow - setPredictionIdxCol***:: = %s" % (dbm.m_singleqsar_prediction_IdxCol))

    def selectPredictionOutputFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - Prediction", "", 'Comma-Separated Values Files (*.csv)')
        self.ui_singleqsar_prediction_outputfile_txtline.setText(filename)

    def setPredictionOutputFile(self):
        if self.ui_singleqsar_prediction_outputfile_txtline.text():
            dbm.m_singleqsar_prediction_outputfile = str(self.ui_singleqsar_prediction_outputfile_txtline.text())
            dbm.m_singleqsar_prediction_outputfile.replace(" ", "")
            dbm.ui_bool_singleqsar_prediction_outputfile_txtline = True
            self.loadEnabled()
            print ("\n*** setPredictionOutputFile*** :: = %s"  % (dbm.m_singleqsar_prediction_outputfile))

    def importPrediction_Dataset(self):
        try:
            # Import data from other functions
            TrainX = dbm.m_training_dataset_TrainX

            # Import data from csv into pandas
            dataPredict = pd.read_csv(dbm.m_singleqsar_prediction_dataset, header = dbm.m_singleqsar_prediction_header, index_col = dbm.m_singleqsar_prediction_IdxCol)

            # Extract Activity Values
            # app.py = bpsorand.py || dataTrainX = xTrain   dataValidationX = xValidation
            yPredict = dataPredict.iloc[:, 0].values
            PredictX = dataPredict

            if dbm.m_dataset_std == True:
                mean = TrainX.mean().values
                std = TrainX.std().values
                if dbm.m_dataset_std == True:
                    # Double Check (basing off of eric su's code not gene's sample
                    xPredict = ((PredictX - mean)/std).values

            elif dbm.m_dataset_rescale == True:
                min = TrainX.min().values
                max = TrainX.max().values
                if dbm.m_dataset_rescale == True:
                    xPredict = ((PredictX - min)/(max - min)).values

            else:
                xPredict = PredictX.values

            dbm.m_singleqsar_prediction_dataset_x = xPredict
            dbm.m_singleqsar_prediction_dataset_y = yPredict
            dbm.m_singleqsar_prediction_datapredict = dataPredict

            # Enable Run button after dataset is imported
            dbm.ui_bool_singleqsar_prediction_run_btn = True
            self.loadEnabled()
            QtGui.QMessageBox.about(self, 'Success','Dataset has been successfully imported!')
        except Exception:
            QtGui.QMessageBox.about(self, 'Failure','Dataset import has failed!')

    def runPredictionScript(self):
        try:
            QtGui.QMessageBox.about(self, 'Run','Prediction calculations has started.\nThis may take some time...\nPlease click \'ok\' and wait for the completion popup to appear  ')
            evoqsartestscript.evoQSARPrediction(dbm)
            QtGui.QMessageBox.about(self, 'Success','Your calculation was successful!')
        except Exception:
            QtGui.QMessageBox.about(self, 'Failure','A failure occurred, please re-check your values and run again.')

    # Uses Python's pickle function to save the database (pickle file)
    def saveObject(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save - evoQSAR Project", "", 'QSAR Project (*.qsar)')
        if filename:    # If operation is not cancelled, save QSAR project
            evoqsardbm.save_object(dbm, filename)
            print dbm.enabledTabs

    # Uses Python's pickle function to load the database (pickle file)
    def loadObject(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Open - evoQSAR Project", "", 'QSAR Project (*.qsar)')
        if filename:    # If operation is not cancelled, open QSAR project
            global dbm
            dbm = evoqsardbm.load_object(filename)
            dbm.m_yRand_user_input = False # Needs to be set to false, so that text setting for yRand does not trigger a save
            print evoqsardbm.load_object(filename).__dict__
            print "Loaded DBM"

            self.loadTabs(dbm.enabledTabs)
            self.loadEnabled()
            print "Loaded GUI"

    # Loads Enabled Tabs in the GUI
    def loadTabs(self, tabList):
        i = 0
        for tab in tabList:
            self.ui_tab_widget.setTabEnabled(i, tab)
            i = i + 1

    # Loads all user display settings (Refreshes GUI Environment once something is changed)
    # **Note** PyQt(GUI) elements are not pickle-able, thus one needs to separate
    # user inputted data and the GUI elements
    def loadEnabled(self):

        # Default Items
        # grey out certain parameters on initialization
        self.ui_ncomponents_txtline.setEnabled(False)
        self.ui_cv_nfold_txtline.setEnabled(False)
        self.ui_cv_shuffle_chkbox.setEnabled(False)
        self.ui_cv_shuffle_nruns_txtline.setEnabled(False)
        self.ui_singleqsar_prediction_run_btn.setEnabled(False)
        self.ui_stepwise_run_btn.setEnabled(False)
        self.ui_singleqsar_ad_init_btn.setEnabled(False)
        self.ui_singleqsar_yrand_outputfile_btn.setEnabled(False)
        self.ui_singleqsar_yrand_init_btn.setEnabled(False)

        # Disable all user-input text edits (only rely on prompt)
        self.ui_trainingset_txtline.setEnabled(False)
        self.ui_validationset_txtline.setEnabled(False)
        self.ui_testset_txtline.setEnabled(False)
        self.ui_results_models_outputfile_txtedit.setEnabled(False)
        self.ui_results_importance_outputfile_txtedit.setEnabled(False)
        self.ui_testset_txtline_2.setEnabled(False)
        self.ui_stepwise_outputfile_txtline.setEnabled(False)
        self.ui_singleqsar_ad_trainoutputfile_txtline.setEnabled(False)
        self.ui_singleqsar_ad_testoutputfile_txtline.setEnabled(False)
        self.ui_singleqsar_yrand_init_btn.setEnabled(False)
        self.ui_singleqsar_yrand_outputfile_txtline.setEnabled(False)
        self.ui_singleqsar_statistics_outputfile_txtline.setEnabled(False)
        self.ui_singleqsar_prediction_dataset_txtline.setEnabled(False)
        self.ui_singleqsar_prediction_outputfile_txtline.setEnabled(False)

        # User Modified Parameters
        # setEnabled()
        self.ui_trainingbrowse_btn.setEnabled(dbm.ui_bool_trainingbrowse_btn)
        self.ui_validationbrowse_btn.setEnabled(dbm.ui_bool_validationbrowse_btn)
        self.ui_testbrowse_btn.setEnabled(dbm.ui_bool_testbrowse_btn)

        self.ui_trainingset_txtline.setEnabled(dbm.ui_bool_trainingset_txtline)
        self.ui_trainingset_txtline.setReadOnly(dbm.ui_bool_trainingset_txtline)
        self.ui_validationset_txtline.setEnabled(dbm.ui_bool_validationset_txtline)
        self.ui_validationset_txtline.setReadOnly(dbm.ui_bool_validationset_txtline)
        self.ui_testset_txtline.setEnabled(dbm.ui_bool_testset_txtline)
        self.ui_testset_txtline.setReadOnly(dbm.ui_bool_testset_txtline)

        self.ui_alpha_txtline_1.setEnabled(dbm.ui_bool_alpha_txtline_1)
        self.ui_alpha_txtline_2.setEnabled(dbm.ui_bool_alpha_txtline_2)
        self.ui_beta_txtline.setEnabled(dbm.ui_bool_beta_txtline)
        self.ui_f_txtline.setEnabled(dbm.ui_bool_f_txtline)
        self.ui_cr_txtline.setEnabled(dbm.ui_bool_cr_txtline)

        self.ui_ncomponents_txtline.setEnabled(dbm.ui_bool_ncomponents_txtline)
        self.ui_cv_nfold_txtline.setEnabled(dbm.ui_bool_cv_nfold_txtline)
        self.ui_cv_shuffle_chkbox.setEnabled(dbm.ui_bool_cv_shuffle_chkbox)
        self.ui_cv_shuffle_nruns_txtline.setEnabled(dbm.ui_bool_cv_shuffle_nruns_txtline)

        self.ui_results_models_outputfile_txtedit.setEnabled(dbm.ui_bool_results_models_outputfile_txtedit)
        self.ui_results_models_outputfile_txtedit.setReadOnly(dbm.ui_bool_results_models_outputfile_txtedit)

        self.ui_results_importance_outputfile_txtedit.setEnabled(dbm.ui_bool_results_importance_outputfile_txtedit)
        self.ui_results_importance_outputfile_txtedit.setReadOnly(dbm.ui_bool_results_importance_outputfile_txtedit)

        self.ui_testset_txtline_2.setEnabled(dbm.ui_bool_testset_txtline_2)
        self.ui_testset_txtline_2.setReadOnly(dbm.ui_bool_testset_txtline_2)

        self.ui_stepwise_run_btn.setEnabled(dbm.ui_bool_stepwise_run_btn)

        self.ui_stepwise_outputfile_txtline.setEnabled(dbm.ui_bool_stepwise_outputfile_txtline)
        self.ui_stepwise_outputfile_txtline.setReadOnly(dbm.ui_bool_stepwise_outputfile_txtline)

        self.ui_singleqsar_ad_trainoutputfile_txtline.setEnabled(dbm.ui_bool_singleqsar_ad_trainoutputfile_txtline)
        self.ui_singleqsar_ad_trainoutputfile_txtline.setReadOnly(dbm.ui_bool_singleqsar_ad_trainoutputfile_txtline)

        self.ui_singleqsar_ad_testoutputfile_txtline.setEnabled(dbm.ui_bool_singleqsar_ad_testoutputfile_txtline)
        self.ui_singleqsar_ad_testoutputfile_txtline.setReadOnly(dbm.ui_bool_singleqsar_ad_testoutputfile_txtline)
        self.ui_singleqsar_ad_init_btn.setEnabled(dbm.ui_bool_singleqsar_ad_init_btn)

        self.ui_singleqsar_yrand_outputfile_btn.setEnabled(dbm.ui_bool_singleqsar_yrand_outputfile_btn)
        self.ui_singleqsar_yrand_init_btn.setEnabled(dbm.ui_bool_singleqsar_yrand_init_btn)

        self.ui_singleqsar_yrand_outputfile_txtline.setEnabled(dbm.ui_bool_singleqsar_yrand_outputfile_txtline)
        self.ui_singleqsar_yrand_outputfile_txtline.setReadOnly(dbm.ui_bool_singleqsar_yrand_outputfile_txtline)

        self.ui_singleqsar_statistics_outputfile_txtline.setEnabled(dbm.ui_bool_singleqsar_statistics_outputfile_txtline)
        self.ui_singleqsar_statistics_outputfile_txtline.setReadOnly(dbm.ui_bool_singleqsar_statistics_outputfile_txtline)

        self.ui_singleqsar_prediction_dataset_txtline.setEnabled(dbm.ui_bool_singleqsar_prediction_dataset_txtline)
        self.ui_singleqsar_prediction_dataset_txtline.setReadOnly(dbm.ui_bool_singleqsar_prediction_dataset_txtline)

        self.ui_singleqsar_prediction_outputfile_txtline.setEnabled(dbm.ui_bool_singleqsar_prediction_outputfile_txtline)
        self.ui_singleqsar_prediction_outputfile_txtline.setReadOnly(dbm.ui_bool_singleqsar_prediction_outputfile_txtline)

        self.ui_singleqsar_prediction_run_btn.setEnabled(dbm.ui_bool_singleqsar_prediction_run_btn)

        #setText()
        if dbm.m_training_dataset:
            self.ui_trainingset_txtline.setText(dbm.m_training_dataset)
        if dbm.m_validation_dataset:
            self.ui_validationset_txtline.setText(dbm.m_validation_dataset)
        if dbm.m_test_dataset:
            self.ui_testset_txtline.setText(dbm.m_test_dataset)
        if dbm.m_results_models_outputfile:
            self.ui_results_models_outputfile_txtedit.setText(dbm.m_results_models_outputfile)
        if dbm.m_results_importance_outputfile:
            self.ui_results_importance_outputfile_txtedit.setText(dbm.m_results_importance_outputfile)
        if dbm.m_test_dataset_2:
            self.ui_testset_txtline_2.setText(dbm.m_test_dataset_2)
        if dbm.m_stepwise_outputfile:
            self.ui_stepwise_outputfile_txtline.setText(dbm.m_stepwise_outputfile)
        if dbm.m_singleqsar_ad_trainoutputfile:
            self.ui_singleqsar_ad_trainoutputfile_txtline.setText(dbm.m_singleqsar_ad_trainoutputfile)
        if dbm.m_singleqsar_ad_testoutputfile:
            self.ui_singleqsar_ad_testoutputfile_txtline.setText(dbm.m_singleqsar_ad_testoutputfile)
        if dbm.m_singleqsar_yrand_outputfile:
            self.ui_singleqsar_yrand_outputfile_txtline.setText(dbm.m_singleqsar_yrand_outputfile)
        if dbm.m_singleqsar_statistics_outputfile:
            self.ui_singleqsar_statistics_outputfile_txtline.setText(dbm.m_singleqsar_statistics_outputfile)
        if dbm.m_singleqsar_outputStats:
            self.ui_singleqsar_statistics_txtedit.setText(dbm.m_singleqsar_outputStats)
        if dbm.m_singleqsar_prediction_dataset:
            self.ui_singleqsar_prediction_dataset_txtline.setText(dbm.m_singleqsar_prediction_dataset)
        if dbm.m_singleqsar_prediction_outputfile:
            self.ui_singleqsar_prediction_outputfile_txtline.setText(dbm.m_singleqsar_prediction_outputfile)

        #setChecked()
        if dbm.gui_mode == 'stepqsar':
            self.ui_stepqsar_radiobtn.setChecked(True)
        if dbm.gui_mode == 'singleqsar':
            self.ui_singleqsar_radiobtn.setChecked(True)

        if dbm.m_dataset_header == 0:
            self.ui_datasetheader_chkbox.setChecked(True)
        if dbm.m_dataset_idxcol == 0:
            self.ui_datasetidxcol_chkbox.setChecked(True)

        if dbm.m_parallel_param == -1:
            self.ui_parallel_chkbox.setChecked(True)

        if dbm.m_cv_shuffle_chkbox == 0:
            self.ui_cv_shuffle_chkbox.setChecked(True)

        if dbm.m_dataset_header_2 == 0:
            self.ui_datasetheader_chkbox_2.setChecked(True)
        if dbm.m_dataset_idxcol_2 == 0:
            self.ui_datasetidxcol_chkbox_2.setChecked(True)

        if dbm.m_singleqsar_prediction_header == 0:
            self.ui_singleqsar_prediction_header_chkbox.setChecked(True)
        if dbm.m_singleqsar_prediction_IdxCol == 0:
            self.ui_singleqsar_prediction_idxcol_chkbox.setChecked(True)


        #comboBox
        if dbm.m_dataset_extra == "None":
            self.ui_dataset_param_combobox.setCurrentIndex(\
                self.ui_dataset_param_combobox.findText("None"))
        if dbm.m_dataset_extra == "Rescale":
            self.ui_dataset_param_combobox.setCurrentIndex(\
                self.ui_dataset_param_combobox.findText("Rescale"))
        if dbm.m_dataset_extra == "Standardize":
            self.ui_dataset_param_combobox.setCurrentIndex(\
                self.ui_dataset_param_combobox.findText("Standardize"))

        if dbm.m_feature_selection == "BPSO":
            self.ui_featureselection_combobox.setCurrentIndex(\
                self.ui_featureselection_combobox.findText("BPSO"))
        if dbm.m_feature_selection == "DE-BPSO":
            self.ui_featureselection_combobox.setCurrentIndex(\
                self.ui_featureselection_combobox.findText("DE-BPSO"))

        if dbm.m_modeling_method == "MLR":
            self.ui_modelingmethod_combobox.setCurrentIndex(\
                self.ui_modelingmethod_combobox.findText("MLR"))
        if dbm.m_modeling_method == "PLSR":
            self.ui_modelingmethod_combobox.setCurrentIndex(\
                self.ui_modelingmethod_combobox.findText("PLSR"))

        if dbm.m_paramcv_value == "Leave one out":
            self.ui_cv_combobox.setCurrentIndex(\
                self.ui_cv_combobox.findText("Leave one out"))
        if dbm.m_paramcv_value == "K-Fold":
            self.ui_cv_combobox.setCurrentIndex(\
                self.ui_cv_combobox.findText("K-Fold"))




    # Prepopulates GUI with default values (used on first load)
    def loadPrePopulate(self):

        self.ui_alpha_txtline_1.setText(str(dbm.m_alpha1_param))
        self.ui_alpha_txtline_2.setText(str(dbm.m_alpha2_param))
        self.ui_beta_txtline.setText(str(dbm.m_beta_param))
        self.ui_f_txtline.setText(str(dbm.m_f_param))
        self.ui_cr_txtline.setText(str(dbm.m_cr_param))

        self.ui_initfeat_txtline.setText(str(dbm.m_initfeat_param))
        self.ui_popsize_txtline.setText(str(dbm.m_popsize_param))
        self.ui_maxgen_txtline.setText(str(dbm.m_maxgen_param))
        self.ui_maxmodels_txtline.setText(str(dbm.m_maxmodels_param))
        self.ui_penaltyfactor_txtline.setText(str(dbm.m_penaltyfactor_param))

        self.ui_cv_nfold_txtline.setText(str(dbm.m_cv_nfold_param))
        self.ui_cv_shuffle_nruns_txtline.setText(str(dbm.m_cv_shuffle_nruns_param))
        self.ui_maxdescript_txtline.setText(str(dbm.m_max_descript))
        self.ui_singleqsar_yrand_nruns_txtline.setText(str(dbm.m_singleqsar_yrand_nruns))











