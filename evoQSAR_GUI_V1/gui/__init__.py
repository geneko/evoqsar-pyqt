__author__ = 'ESU_2'

""" Sub-package containing all user interface components for evoQSAR"""

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = []
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

import os.path

import PyQt4.uic
from PyQt4 import QtCore

def loadUi( modpath, widget ):
    """
    Uses the PyQt4.uic.loadUI method to load the inputted ui file associated
    with the given module path and widget class information on the inputted
    widget.

    :param      modpath | str
    :param      widget  | QWidget
    """
    # generate the uifile path
    basepath = os.path.dirname(modpath)
    basename = widget.__class__.__name__.lower()
    uifile   = os.path.join(basepath, 'ui/%s.ui' % basename)
    uipath   = os.path.dirname(uifile)

    # swap the current path to use the ui file's path
    currdir = QtCore.QDir.currentPath()
    QtCore.QDir.setCurrent(uipath)

    # print ("\n*** GUI INIT *** ::\nbasepath = %s\nbasename = %s\nuifile = %s\nuipath = %s\ncurrdir = %s"
    #        % (basepath, basename, uifile, uipath, currdir))

    # load the ui
    PyQt4.uic.loadUi(uifile, widget)

    # reset the current QDir path
    QtCore.QDir.setCurrent(currdir)

