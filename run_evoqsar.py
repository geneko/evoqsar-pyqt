""" Main entry point to the evoQSAR application.  """

# define authorship information
__authors__     = ['Eric Su']
__author__      = ','.join(__authors__)
__credits__     = ['Gene Ko']
__copyright__   = 'Copyright (c) 2015'
__license__     = 'GPL'

# maintanence information
__maintainer__  = 'Eric Su'
__email__       = 'su.eric.09@gmail.com'

import ctypes
import os
import sys

from PyQt4 import QtGui
from evoQSAR_GUI_V1.gui import evoqsarmainwindow

def main(argv = None):
    """
    Creates the main window for the evoQSAR application and begins the \
    QApplication if necessary.

    :param      argv | [, ..] || None

    :return      error code
    """
    app = None

    # Set working directory to user home directory
    os.chdir(os.path.expanduser('~'))

    # create the application if necessary
    if ( not QtGui.QApplication.instance() ):
        app = QtGui.QApplication(argv)
        app.setStyle('plastique')

    # create the main window
    window = evoqsarmainwindow.evoQSARMainWindow()
    window.show()

    # run the application if necessary
    if ( app ):
        return app.exec_()

    # no errors since we're not running our own event loop
    return 0

if ( __name__ == '__main__' ):
    # Get missing MKL library for Anaconda 2.5 and PyInstaller 1.3
    # https://github.com/pyinstaller/pyinstaller/wiki/Recipe-Win-Load-External-DLL
    try:
        ctypes.CDLL('mkl_def.dll')
    except:
        pass

    sys.exit(main(sys.argv))


